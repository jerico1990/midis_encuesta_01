<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>MIDIS |  Encuesta</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="MIDIS |  Encuesta" name="description">
        <meta content="MIDIS" name="author">
        <!-- App favicon -->
        <link rel="shortcut icon" href="<?= \Yii::$app->request->BaseUrl ?>/img/icon.jpg">

        <!-- Bootstrap Css -->
        <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/css/bootstrap.min.css"  rel="stylesheet" type="text/css">
        <!-- Icons Css -->
        <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/css/icons.min.css" rel="stylesheet" type="text/css">
        <!-- App Css-->
        <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/css/app.min.css"  rel="stylesheet" type="text/css">
        <!-- Custom Css-->
        <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/css/custom.css" rel="stylesheet" type="text/css">

        <style type="text/css">
            .apexcharts-canvas {
                position: relative;
                user-select: none;
            /* cannot give overflow: hidden as it will crop tooltips which overflow outside chart area */
            }


            /* scrollbar is not visible by default for legend, hence forcing the visibility */
            .apexcharts-canvas ::-webkit-scrollbar {
                -webkit-appearance: none;
                width: 6px;
            }

            .apexcharts-canvas ::-webkit-scrollbar-thumb {
                border-radius: 4px;
                background-color: rgba(0, 0, 0, .5);
                box-shadow: 0 0 1px rgba(255, 255, 255, .5);
                -webkit-box-shadow: 0 0 1px rgba(255, 255, 255, .5);
            }


            .apexcharts-inner {
                position: relative;
            }

            .apexcharts-text tspan {
                font-family: inherit;
            }

            .legend-mouseover-inactive {
                transition: 0.15s ease all;
                opacity: 0.20;
            }

            .apexcharts-series-collapsed {
                opacity: 0;
            }

            .apexcharts-tooltip {
                border-radius: 5px;
                box-shadow: 2px 2px 6px -4px #999;
                cursor: default;
                font-size: 14px;
                left: 62px;
                opacity: 0;
                pointer-events: none;
                position: absolute;
                top: 20px;
                display: flex;
                flex-direction: column;
                overflow: hidden;
                white-space: nowrap;
                z-index: 12;
                transition: 0.15s ease all;
            }

            .apexcharts-tooltip.apexcharts-active {
                opacity: 1;
                transition: 0.15s ease all;
            }

            .apexcharts-tooltip.apexcharts-theme-light {
                border: 1px solid #e3e3e3;
                background: rgba(255, 255, 255, 0.96);
            }

            .apexcharts-tooltip.apexcharts-theme-dark {
                color: #fff;
                background: rgba(30, 30, 30, 0.8);
            }

            .apexcharts-tooltip * {
                font-family: inherit;
            }


            .apexcharts-tooltip-title {
                padding: 6px;
                font-size: 15px;
                margin-bottom: 4px;
            }

            .apexcharts-tooltip.apexcharts-theme-light .apexcharts-tooltip-title {
                background: #ECEFF1;
                border-bottom: 1px solid #ddd;
            }

            .apexcharts-tooltip.apexcharts-theme-dark .apexcharts-tooltip-title {
                background: rgba(0, 0, 0, 0.7);
                border-bottom: 1px solid #333;
            }

            .apexcharts-tooltip-text-value,
            .apexcharts-tooltip-text-z-value {
                display: inline-block;
                font-weight: 600;
                margin-left: 5px;
            }

            .apexcharts-tooltip-text-z-label:empty,
            .apexcharts-tooltip-text-z-value:empty {
                display: none;
            }

            .apexcharts-tooltip-text-value,
            .apexcharts-tooltip-text-z-value {
                font-weight: 600;
            }

            .apexcharts-tooltip-marker {
                width: 12px;
                height: 12px;
                position: relative;
                top: 0px;
                margin-right: 10px;
                border-radius: 50%;
            }

            .apexcharts-tooltip-series-group {
                padding: 0 10px;
                display: none;
                text-align: left;
                justify-content: left;
                align-items: center;
            }

            .apexcharts-tooltip-series-group.apexcharts-active .apexcharts-tooltip-marker {
                opacity: 1;
            }

            .apexcharts-tooltip-series-group.apexcharts-active,
            .apexcharts-tooltip-series-group:last-child {
                padding-bottom: 4px;
            }

            .apexcharts-tooltip-series-group-hidden {
                opacity: 0;
                height: 0;
                line-height: 0;
                padding: 0 !important;
            }

            .apexcharts-tooltip-y-group {
                padding: 6px 0 5px;
            }

            .apexcharts-tooltip-candlestick {
                padding: 4px 8px;
            }

            .apexcharts-tooltip-candlestick>div {
                margin: 4px 0;
            }

            .apexcharts-tooltip-candlestick span.value {
                font-weight: bold;
            }

            .apexcharts-tooltip-rangebar {
                padding: 5px 8px;
            }

            .apexcharts-tooltip-rangebar .category {
                font-weight: 600;
                color: #777;
            }

            .apexcharts-tooltip-rangebar .series-name {
                font-weight: bold;
                display: block;
                margin-bottom: 5px;
            }

            .apexcharts-xaxistooltip {
                opacity: 0;
                padding: 9px 10px;
                pointer-events: none;
                color: #373d3f;
                font-size: 13px;
                text-align: center;
                border-radius: 2px;
                position: absolute;
                z-index: 10;
                background: #ECEFF1;
                border: 1px solid #90A4AE;
                transition: 0.15s ease all;
            }

            .apexcharts-xaxistooltip.apexcharts-theme-dark {
                background: rgba(0, 0, 0, 0.7);
                border: 1px solid rgba(0, 0, 0, 0.5);
                color: #fff;
            }

            .apexcharts-xaxistooltip:after,
            .apexcharts-xaxistooltip:before {
                left: 50%;
                border: solid transparent;
                content: " ";
                height: 0;
                width: 0;
                position: absolute;
                pointer-events: none;
            }

            .apexcharts-xaxistooltip:after {
                border-color: rgba(236, 239, 241, 0);
                border-width: 6px;
                margin-left: -6px;
            }

            .apexcharts-xaxistooltip:before {
                border-color: rgba(144, 164, 174, 0);
                border-width: 7px;
                margin-left: -7px;
            }

            .apexcharts-xaxistooltip-bottom:after,
            .apexcharts-xaxistooltip-bottom:before {
                bottom: 100%;
            }

            .apexcharts-xaxistooltip-top:after,
            .apexcharts-xaxistooltip-top:before {
                top: 100%;
            }

            .apexcharts-xaxistooltip-bottom:after {
                border-bottom-color: #ECEFF1;
            }

            .apexcharts-xaxistooltip-bottom:before {
                border-bottom-color: #90A4AE;
            }

            .apexcharts-xaxistooltip-bottom.apexcharts-theme-dark:after {
                border-bottom-color: rgba(0, 0, 0, 0.5);
            }

            .apexcharts-xaxistooltip-bottom.apexcharts-theme-dark:before {
                border-bottom-color: rgba(0, 0, 0, 0.5);
            }

            .apexcharts-xaxistooltip-top:after {
                border-top-color: #ECEFF1
            }

            .apexcharts-xaxistooltip-top:before {
                border-top-color: #90A4AE;
            }

            .apexcharts-xaxistooltip-top.apexcharts-theme-dark:after {
                border-top-color: rgba(0, 0, 0, 0.5);
            }

            .apexcharts-xaxistooltip-top.apexcharts-theme-dark:before {
                border-top-color: rgba(0, 0, 0, 0.5);
            }

            .apexcharts-xaxistooltip.apexcharts-active {
                opacity: 1;
                transition: 0.15s ease all;
            }

            .apexcharts-yaxistooltip {
                opacity: 0;
                padding: 4px 10px;
                pointer-events: none;
                color: #373d3f;
                font-size: 13px;
                text-align: center;
                border-radius: 2px;
                position: absolute;
                z-index: 10;
                background: #ECEFF1;
                border: 1px solid #90A4AE;
            }

            .apexcharts-yaxistooltip.apexcharts-theme-dark {
                background: rgba(0, 0, 0, 0.7);
                border: 1px solid rgba(0, 0, 0, 0.5);
                color: #fff;
            }

            .apexcharts-yaxistooltip:after,
            .apexcharts-yaxistooltip:before {
                top: 50%;
                border: solid transparent;
                content: " ";
                height: 0;
                width: 0;
                position: absolute;
                pointer-events: none;
            }

            .apexcharts-yaxistooltip:after {
                border-color: rgba(236, 239, 241, 0);
                border-width: 6px;
                margin-top: -6px;
            }

            .apexcharts-yaxistooltip:before {
                border-color: rgba(144, 164, 174, 0);
                border-width: 7px;
                margin-top: -7px;
            }

            .apexcharts-yaxistooltip-left:after,
            .apexcharts-yaxistooltip-left:before {
                left: 100%;
            }

            .apexcharts-yaxistooltip-right:after,
            .apexcharts-yaxistooltip-right:before {
                right: 100%;
            }

            .apexcharts-yaxistooltip-left:after {
                border-left-color: #ECEFF1;
            }

            .apexcharts-yaxistooltip-left:before {
                border-left-color: #90A4AE;
            }

            .apexcharts-yaxistooltip-left.apexcharts-theme-dark:after {
                border-left-color: rgba(0, 0, 0, 0.5);
            }

            .apexcharts-yaxistooltip-left.apexcharts-theme-dark:before {
                border-left-color: rgba(0, 0, 0, 0.5);
            }

            .apexcharts-yaxistooltip-right:after {
                border-right-color: #ECEFF1;
            }

            .apexcharts-yaxistooltip-right:before {
                border-right-color: #90A4AE;
            }

            .apexcharts-yaxistooltip-right.apexcharts-theme-dark:after {
                border-right-color: rgba(0, 0, 0, 0.5);
            }

            .apexcharts-yaxistooltip-right.apexcharts-theme-dark:before {
                border-right-color: rgba(0, 0, 0, 0.5);
            }

            .apexcharts-yaxistooltip.apexcharts-active {
                opacity: 1;
            }

            .apexcharts-yaxistooltip-hidden {
                display: none;
            }

            .apexcharts-xcrosshairs,
            .apexcharts-ycrosshairs {
                pointer-events: none;
                opacity: 0;
                transition: 0.15s ease all;
            }

            .apexcharts-xcrosshairs.apexcharts-active,
            .apexcharts-ycrosshairs.apexcharts-active {
                opacity: 1;
                transition: 0.15s ease all;
            }

            .apexcharts-ycrosshairs-hidden {
                opacity: 0;
            }

            .apexcharts-selection-rect {
                cursor: move;
            }

            .svg_select_boundingRect, .svg_select_points_rot {
                pointer-events: none;
                opacity: 0;
                visibility: hidden;
            }
            .apexcharts-selection-rect + g .svg_select_boundingRect,
            .apexcharts-selection-rect + g .svg_select_points_rot {
                opacity: 0;
                visibility: hidden;
            }

            .apexcharts-selection-rect + g .svg_select_points_l,
            .apexcharts-selection-rect + g .svg_select_points_r {
                cursor: ew-resize;
                opacity: 1;
                visibility: visible;
            }

            .svg_select_points {
                fill: #efefef;
                stroke: #333;
                rx: 2;
            }

            .apexcharts-svg.apexcharts-zoomable.hovering-zoom {
                cursor: crosshair
            }

            .apexcharts-svg.apexcharts-zoomable.hovering-pan {
                cursor: move
            }

            .apexcharts-zoom-icon,
            .apexcharts-zoomin-icon,
            .apexcharts-zoomout-icon,
            .apexcharts-reset-icon,
            .apexcharts-pan-icon,
            .apexcharts-selection-icon,
            .apexcharts-menu-icon,
            .apexcharts-toolbar-custom-icon {
                cursor: pointer;
                width: 20px;
                height: 20px;
                line-height: 24px;
                color: #6E8192;
                text-align: center;
            }

            .apexcharts-zoom-icon svg,
            .apexcharts-zoomin-icon svg,
            .apexcharts-zoomout-icon svg,
            .apexcharts-reset-icon svg,
            .apexcharts-menu-icon svg {
                fill: #6E8192;
            }

            .apexcharts-selection-icon svg {
                fill: #444;
                transform: scale(0.76)
            }

            .apexcharts-theme-dark .apexcharts-zoom-icon svg,
            .apexcharts-theme-dark .apexcharts-zoomin-icon svg,
            .apexcharts-theme-dark .apexcharts-zoomout-icon svg,
            .apexcharts-theme-dark .apexcharts-reset-icon svg,
            .apexcharts-theme-dark .apexcharts-pan-icon svg,
            .apexcharts-theme-dark .apexcharts-selection-icon svg,
            .apexcharts-theme-dark .apexcharts-menu-icon svg,
            .apexcharts-theme-dark .apexcharts-toolbar-custom-icon svg {
                fill: #f3f4f5;
            }

            .apexcharts-canvas .apexcharts-zoom-icon.apexcharts-selected svg,
            .apexcharts-canvas .apexcharts-selection-icon.apexcharts-selected svg,
            .apexcharts-canvas .apexcharts-reset-zoom-icon.apexcharts-selected svg {
                fill: #008FFB;
            }

            .apexcharts-theme-light .apexcharts-selection-icon:not(.apexcharts-selected):hover svg,
            .apexcharts-theme-light .apexcharts-zoom-icon:not(.apexcharts-selected):hover svg,
            .apexcharts-theme-light .apexcharts-zoomin-icon:hover svg,
            .apexcharts-theme-light .apexcharts-zoomout-icon:hover svg,
            .apexcharts-theme-light .apexcharts-reset-icon:hover svg,
            .apexcharts-theme-light .apexcharts-menu-icon:hover svg {
                fill: #333;
            }

            .apexcharts-selection-icon,
            .apexcharts-menu-icon {
                position: relative;
            }

            .apexcharts-reset-icon {
                margin-left: 5px;
            }

            .apexcharts-zoom-icon,
            .apexcharts-reset-icon,
            .apexcharts-menu-icon {
                transform: scale(0.85);
            }

            .apexcharts-zoomin-icon,
            .apexcharts-zoomout-icon {
                transform: scale(0.7)
            }

            .apexcharts-zoomout-icon {
                margin-right: 3px;
            }

            .apexcharts-pan-icon {
                transform: scale(0.62);
                position: relative;
                left: 1px;
                top: 0px;
            }

            .apexcharts-pan-icon svg {
                fill: #fff;
                stroke: #6E8192;
                stroke-width: 2;
            }

            .apexcharts-pan-icon.apexcharts-selected svg {
                stroke: #008FFB;
            }

            .apexcharts-pan-icon:not(.apexcharts-selected):hover svg {
                stroke: #333;
            }

            .apexcharts-toolbar {
                position: absolute;
                z-index: 11;
                max-width: 176px;
                text-align: right;
                border-radius: 3px;
                padding: 0px 6px 2px 6px;
                display: flex;
                justify-content: space-between;
                align-items: center;
            }

            .apexcharts-menu {
                background: #fff;
                position: absolute;
                top: 100%;
                border: 1px solid #ddd;
                border-radius: 3px;
                padding: 3px;
                right: 10px;
                opacity: 0;
                min-width: 110px;
                transition: 0.15s ease all;
                pointer-events: none;
            }

            .apexcharts-menu.apexcharts-menu-open {
                opacity: 1;
                pointer-events: all;
                transition: 0.15s ease all;
            }

            .apexcharts-menu-item {
                padding: 6px 7px;
                font-size: 12px;
                cursor: pointer;
            }

            .apexcharts-theme-light .apexcharts-menu-item:hover {
                background: #eee;
            }

            .apexcharts-theme-dark .apexcharts-menu {
                background: rgba(0, 0, 0, 0.7);
                color: #fff;
            }

            @media screen and (min-width: 768px) {
                .apexcharts-canvas:hover .apexcharts-toolbar {
                    opacity: 1;
                }
            }

            .apexcharts-datalabel.apexcharts-element-hidden {
                opacity: 0;
            }

            .apexcharts-pie-label,
            .apexcharts-datalabels,
            .apexcharts-datalabel,
            .apexcharts-datalabel-label,
            .apexcharts-datalabel-value {
                cursor: default;
                pointer-events: none;
            }

            .apexcharts-pie-label-delay {
                opacity: 0;
                animation-name: opaque;
                animation-duration: 0.3s;
                animation-fill-mode: forwards;
                animation-timing-function: ease;
            }

            .apexcharts-canvas .apexcharts-element-hidden {
                opacity: 0;
            }

            .apexcharts-hide .apexcharts-series-points {
                opacity: 0;
            }

            .apexcharts-gridline,
            .apexcharts-annotation-rect,
            .apexcharts-tooltip .apexcharts-marker,
            .apexcharts-area-series .apexcharts-area,
            .apexcharts-line,
            .apexcharts-zoom-rect,
            .apexcharts-toolbar svg,
            .apexcharts-area-series .apexcharts-series-markers .apexcharts-marker.no-pointer-events,
            .apexcharts-line-series .apexcharts-series-markers .apexcharts-marker.no-pointer-events,
            .apexcharts-radar-series path,
            .apexcharts-radar-series polygon {
                pointer-events: none;
            }


            /* markers */

            .apexcharts-marker {
                transition: 0.15s ease all;
            }

            @keyframes opaque {
                0% {
                    opacity: 0;
                }
                100% {
                    opacity: 1;
                }
            }


            /* Resize generated styles */

            @keyframes resizeanim {
                from {
                    opacity: 0;
                }
                to {
                    opacity: 0;
                }
            }

            .resize-triggers {
                animation: 1ms resizeanim;
                visibility: hidden;
                opacity: 0;
            }

            .resize-triggers,
            .resize-triggers>div,
            .contract-trigger:before {
                content: " ";
                display: block;
                position: absolute;
                top: 0;
                left: 0;
                height: 100%;
                width: 100%;
                overflow: hidden;
            }

            .resize-triggers>div {
                background: #eee;
                overflow: auto;
            }

            .contract-trigger:before {
                width: 200%;
                height: 200%;
            }

            body[data-layout=horizontal] .page-content{
                margin-top: 10px;
                padding-top: 10px;
            }

            .table{
                font-size: 0.75rem;
            }
        </style>
        <!-- twitter-bootstrap-wizard css -->
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/libs/twitter-bootstrap-wizard/prettify.css">


        <!-- JAVASCRIPT -->
        <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/libs/jquery/jquery.min.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/libs/metismenu/metisMenu.min.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/libs/simplebar/simplebar.min.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/libs/node-waves/waves.min.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/libs/apexcharts/apexcharts.min.js"></script>
        

        <!-- twitter-bootstrap-wizard js -->
        <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/libs/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>

        <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/libs/twitter-bootstrap-wizard/prettify.js"></script>

        <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/libs/parsleyjs/parsley.min.js"></script>


    </head>

    <body data-topbar="dark" data-layout="horizontal" data-layout-size="boxed">

        <!-- Begin page -->
        <div id="layout-wrapper">
<!-- 
            <header id="page-topbar">
                <div class="navbar-header">
                    <div class="d-flex">
                    
                        <form class="app-search d-none d-lg-block">
                            <div class="position-relative">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="bx bx-search-alt"></span>
                            </div>
                        </form>

                        <div class="dropdown dropdown-mega d-none d-lg-block ml-2">
                            <button type="button" class="btn header-item waves-effect" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                                Mega Menu
                                <i class="mdi mdi-chevron-down"></i>
                            </button>
                            <div class="dropdown-menu dropdown-megamenu">
                                <div class="row">
                                    <div class="col-sm-8">

                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5 class="font-size-14 mt-0">UI Components</h5>
                                                <ul class="list-unstyled megamenu-list">
                                                    <li>
                                                        <a href="javascript:void(0);">Lightbox</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Range Slider</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Sweet Alert</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Rating</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Forms</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Tables</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Charts</a>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="col-md-4">
                                                <h5 class="font-size-14 mt-0">Applications</h5>
                                                <ul class="list-unstyled megamenu-list">
                                                    <li>
                                                        <a href="javascript:void(0);">Ecommerce</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Calendar</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Email</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Projects</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Tasks</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Contacts</a>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="col-md-4">
                                                <h5 class="font-size-14 mt-0">Extra Pages</h5>
                                                <ul class="list-unstyled megamenu-list">
                                                    <li>
                                                        <a href="javascript:void(0);">Light Sidebar</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Compact Sidebar</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Horizontal layout</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Maintenance</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Coming Soon</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Timeline</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">FAQs</a>
                                                    </li>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <h5 class="font-size-14 mt-0">UI Components</h5>
                                                <ul class="list-unstyled megamenu-list">
                                                    <li>
                                                        <a href="javascript:void(0);">Lightbox</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Range Slider</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Sweet Alert</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Rating</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Forms</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Tables</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Charts</a>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="col-sm-5">
                                                <div>
                                                    <img src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/images/megamenu-img.png" alt="" class="img-fluid mx-auto d-block">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="d-flex">

                        <div class="dropdown d-inline-block d-lg-none ml-2">
                            <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-search-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="mdi mdi-magnify"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0" aria-labelledby="page-header-search-dropdown">

                                <form class="p-3">
                                    <div class="form-group m-0">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Search ..." aria-label="Recipient's username">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary" type="submit"><i class="mdi mdi-magnify"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="dropdown d-inline-block">
                            <button type="button" class="btn header-item waves-effect" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="" src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/images/flags/us.jpg" alt="Header Language" height="16">
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">

                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <img src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/images/flags/spain.jpg" alt="user-image" class="mr-1" height="12">
                                    <span class="align-middle">Spanish</span>
                                </a>

                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <img src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/images/flags/germany.jpg" alt="user-image" class="mr-1" height="12">
                                    <span class="align-middle">German</span>
                                </a>

                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <img src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/images/flags/italy.jpg" alt="user-image" class="mr-1" height="12">
                                    <span class="align-middle">Italian</span>
                                </a>

                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <img src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/images/flags/russia.jpg" alt="user-image" class="mr-1" height="12">
                                    <span class="align-middle">Russian</span>
                                </a>
                            </div>
                        </div>

                        <div class="dropdown d-none d-lg-inline-block ml-1">
                            <button type="button" class="btn header-item noti-icon waves-effect" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="bx bx-customize"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                                <div class="px-lg-2">
                                    <div class="row no-gutters">
                                        <div class="col">
                                            <a class="dropdown-icon-item" href="#">
                                                <img src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/images/brands/github.png" alt="Github">
                                                <span>GitHub</span>
                                            </a>
                                        </div>
                                        <div class="col">
                                            <a class="dropdown-icon-item" href="#">
                                                <img src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/images/brands/bitbucket.png" alt="bitbucket">
                                                <span>Bitbucket</span>
                                            </a>
                                        </div>
                                        <div class="col">
                                            <a class="dropdown-icon-item" href="#">
                                                <img src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/images/brands/dribbble.png" alt="dribbble">
                                                <span>Dribbble</span>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="row no-gutters">
                                        <div class="col">
                                            <a class="dropdown-icon-item" href="#">
                                                <img src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/images/brands/dropbox.png" alt="dropbox">
                                                <span>Dropbox</span>
                                            </a>
                                        </div>
                                        <div class="col">
                                            <a class="dropdown-icon-item" href="#">
                                                <img src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/images/brands/mail_chimp.png" alt="mail_chimp">
                                                <span>Mail Chimp</span>
                                            </a>
                                        </div>
                                        <div class="col">
                                            <a class="dropdown-icon-item" href="#">
                                                <img src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/images/brands/slack.png" alt="slack">
                                                <span>Slack</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="dropdown d-none d-lg-inline-block ml-1">
                            <button type="button" class="btn header-item noti-icon waves-effect" data-toggle="fullscreen">
                                <i class="bx bx-fullscreen"></i>
                            </button>
                        </div>

                        <div class="dropdown d-inline-block">
                            <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-notifications-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="bx bx-bell bx-tada"></i>
                                <span class="badge badge-danger badge-pill">3</span>
                            </button>
                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0" aria-labelledby="page-header-notifications-dropdown">
                                <div class="p-3">
                                    <div class="row align-items-center">
                                        <div class="col">
                                            <h6 class="m-0"> Notifications </h6>
                                        </div>
                                        <div class="col-auto">
                                            <a href="#!" class="small"> View All</a>
                                        </div>
                                    </div>
                                </div>
                                <div data-simplebar="init" style="max-height: 230px;"><div class="simplebar-wrapper" style="margin: 0px;"><div class="simplebar-height-auto-observer-wrapper"><div class="simplebar-height-auto-observer"></div></div><div class="simplebar-mask"><div class="simplebar-offset" style="right: 0px; bottom: 0px;"><div class="simplebar-content-wrapper" style="height: auto; overflow: hidden;"><div class="simplebar-content" style="padding: 0px;">
                                    <a href="" class="text-reset notification-item">
                                        <div class="media">
                                            <div class="avatar-xs mr-3">
                                                <span class="avatar-title bg-primary rounded-circle font-size-16">
                                                    <i class="bx bx-cart"></i>
                                                </span>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="mt-0 mb-1">Your order is placed</h6>
                                                <div class="font-size-12 text-muted">
                                                    <p class="mb-1">If several languages coalesce the grammar</p>
                                                    <p class="mb-0"><i class="mdi mdi-clock-outline"></i> 3 min ago</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="" class="text-reset notification-item">
                                        <div class="media">
                                            <img src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/images/users/avatar-3.jpg" class="mr-3 rounded-circle avatar-xs" alt="user-pic">
                                            <div class="media-body">
                                                <h6 class="mt-0 mb-1">James Lemire</h6>
                                                <div class="font-size-12 text-muted">
                                                    <p class="mb-1">It will seem like simplified English.</p>
                                                    <p class="mb-0"><i class="mdi mdi-clock-outline"></i> 1 hours ago</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="" class="text-reset notification-item">
                                        <div class="media">
                                            <div class="avatar-xs mr-3">
                                                <span class="avatar-title bg-success rounded-circle font-size-16">
                                                    <i class="bx bx-badge-check"></i>
                                                </span>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="mt-0 mb-1">Your item is shipped</h6>
                                                <div class="font-size-12 text-muted">
                                                    <p class="mb-1">If several languages coalesce the grammar</p>
                                                    <p class="mb-0"><i class="mdi mdi-clock-outline"></i> 3 min ago</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>

                                    <a href="" class="text-reset notification-item">
                                        <div class="media">
                                            <img src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/images/users/avatar-4.jpg" class="mr-3 rounded-circle avatar-xs" alt="user-pic">
                                            <div class="media-body">
                                                <h6 class="mt-0 mb-1">Salena Layfield</h6>
                                                <div class="font-size-12 text-muted">
                                                    <p class="mb-1">As a skeptical Cambridge friend of mine occidental.</p>
                                                    <p class="mb-0"><i class="mdi mdi-clock-outline"></i> 1 hours ago</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div></div></div></div><div class="simplebar-placeholder" style="width: 0px; height: 0px;"></div></div><div class="simplebar-track simplebar-horizontal" style="visibility: hidden;"><div class="simplebar-scrollbar" style="transform: translate3d(0px, 0px, 0px); display: none;"></div></div><div class="simplebar-track simplebar-vertical" style="visibility: hidden;"><div class="simplebar-scrollbar" style="transform: translate3d(0px, 0px, 0px); display: none;"></div></div></div>
                                <div class="p-2 border-top">
                                    <a class="btn btn-sm btn-link font-size-14 btn-block text-center" href="javascript:void(0)">
                                        <i class="mdi mdi-arrow-right-circle mr-1"></i> View More..
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="dropdown d-inline-block">
                            <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="rounded-circle header-profile-user" src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/images/users/avatar-1.jpg" alt="Header Avatar">
                                <span class="d-none d-xl-inline-block ml-1">Henry</span>
                                <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                               
                                <a class="dropdown-item" href="#"><i class="bx bx-user font-size-16 align-middle mr-1"></i>
                                    Profile</a>
                                <a class="dropdown-item" href="#"><i class="bx bx-wallet font-size-16 align-middle mr-1"></i> My Wallet</a>
                                <a class="dropdown-item d-block" href="#"><span class="badge badge-success float-right">11</span><i class="bx bx-wrench font-size-16 align-middle mr-1"></i> Settings</a>
                                <a class="dropdown-item" href="#"><i class="bx bx-lock-open font-size-16 align-middle mr-1"></i> Lock screen</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item text-danger" href="#"><i class="bx bx-power-off font-size-16 align-middle mr-1 text-danger"></i> Logout</a>
                            </div>
                        </div>

                        <div class="dropdown d-inline-block">
                            <button type="button" class="btn header-item noti-icon right-bar-toggle waves-effect">
                                <i class="bx bx-cog bx-spin"></i>
                            </button>
                        </div>

                    </div>
                </div>
            </header> -->
    <!-- 
            <div class="topnav">
                <div class="container-fluid">
                    <nav class="navbar navbar-light navbar-expand-lg topnav-menu">

                        <div class="collapse navbar-collapse" id="topnav-menu-content">
                            <ul class="navbar-nav active">

                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-dashboard" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="bx bx-home-circle mr-2"></i>Dashboard <div class="arrow-down"></div>
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="topnav-dashboard">

                                        <a href="index.html" class="dropdown-item">Default</a>
                                        <a href="dashboard-saas.html" class="dropdown-item">Saas</a>
                                        <a href="dashboard-crypto.html" class="dropdown-item">Crypto</a>
                                        <a href="dashboard-blog.html" class="dropdown-item">Blog</a>
                                    </div>
                                </li>

                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-uielement" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="bx bx-tone mr-2"></i>UI Elements <div class="arrow-down"></div>
                                    </a>

                                    <div class="dropdown-menu mega-dropdown-menu px-2 dropdown-mega-menu-xl" aria-labelledby="topnav-uielement">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div>
                                                    <a href="ui-alerts.html" class="dropdown-item">Alerts</a>
                                                    <a href="ui-buttons.html" class="dropdown-item">Buttons</a>
                                                    <a href="ui-cards.html" class="dropdown-item">Cards</a>
                                                    <a href="ui-carousel.html" class="dropdown-item">Carousel</a>
                                                    <a href="ui-dropdowns.html" class="dropdown-item">Dropdowns</a>
                                                    <a href="ui-grid.html" class="dropdown-item">Grid</a>
                                                    <a href="ui-images.html" class="dropdown-item">Images</a>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div>
                                                    <a href="ui-lightbox.html" class="dropdown-item">Lightbox</a>
                                                    <a href="ui-modals.html" class="dropdown-item">Modals</a>
                                                    <a href="ui-rangeslider.html" class="dropdown-item">Range Slider</a>
                                                    <a href="ui-session-timeout.html" class="dropdown-item">Session
                                                        Timeout</a>
                                                    <a href="ui-progressbars.html" class="dropdown-item">Progress Bars</a>
                                                    <a href="ui-sweet-alert.html" class="dropdown-item">Sweet-Alert</a>
                                                    <a href="ui-tabs-accordions.html" class="dropdown-item">Tabs &amp;
                                                        Accordions</a>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div>
                                                    <a href="ui-typography.html" class="dropdown-item">Typography</a>
                                                    <a href="ui-video.html" class="dropdown-item">Video</a>
                                                    <a href="ui-general.html" class="dropdown-item">General</a>
                                                    <a href="ui-colors.html" class="dropdown-item">Colors</a>
                                                    <a href="ui-rating.html" class="dropdown-item">Rating</a>
                                                    <a href="ui-notifications.html" class="dropdown-item">Notifications</a>
                                                    <a href="ui-image-cropper.html" class="dropdown-item">Image Cropper</a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </li>

                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-pages" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="bx bx-customize mr-2"></i>Apps <div class="arrow-down"></div>
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="topnav-pages">

                                        <a href="calendar.html" class="dropdown-item">Calendar</a>
                                        <a href="chat.html" class="dropdown-item">Chat</a>
                                        <a href="apps-filemanager.html" class="dropdown-item">File Manager</a>
                                        <div class="dropdown">
                                            <a class="dropdown-item dropdown-toggle arrow-none" href="#" id="topnav-email" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Email <div class="arrow-down"></div>
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="topnav-email">
                                                <a href="email-inbox.html" class="dropdown-item">Inbox</a>
                                                <a href="email-read.html" class="dropdown-item">Read Email</a>
                                                <div class="dropdown">
                                                    <a class="dropdown-item dropdown-toggle arrow-none" href="#" id="topnav-blog" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <span>Templates</span>
                                                        <div class="arrow-down"></div>
                                                    </a>
                                                    <div class="dropdown-menu" aria-labelledby="topnav-blog">
                                                        <a href="email-template-basic.html" class="dropdown-item">Basic
                                                            Action</a>
                                                        <a href="email-template-alert.html" class="dropdown-item">Alert
                                                            Email</a>
                                                        <a href="email-template-billing.html" class="dropdown-item">Billing
                                                            Email</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dropdown">
                                            <a class="dropdown-item dropdown-toggle arrow-none" href="#" id="topnav-ecommerce" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Ecommerce <div class="arrow-down"></div>
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="topnav-ecommerce">
                                                <a href="ecommerce-products.html" class="dropdown-item">Products</a>
                                                <a href="ecommerce-product-detail.html" class="dropdown-item">Product
                                                    Detail</a>
                                                <a href="ecommerce-orders.html" class="dropdown-item">Orders</a>
                                                <a href="ecommerce-customers.html" class="dropdown-item">Customers</a>
                                                <a href="ecommerce-cart.html" class="dropdown-item">Cart</a>
                                                <a href="ecommerce-checkout.html" class="dropdown-item">Checkout</a>
                                                <a href="ecommerce-shops.html" class="dropdown-item">Shops</a>
                                                <a href="ecommerce-add-product.html" class="dropdown-item">Add Product</a>
                                            </div>
                                        </div>

                                        <div class="dropdown">
                                            <a class="dropdown-item dropdown-toggle arrow-none" href="#" id="topnav-crypto" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Crypto <div class="arrow-down"></div>
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="topnav-crypto">
                                                <a href="crypto-wallet.html" class="dropdown-item">Wallet</a>
                                                <a href="crypto-buy-sell.html" class="dropdown-item">Buy/Sell</a>
                                                <a href="crypto-exchange.html" class="dropdown-item">Exchange</a>
                                                <a href="crypto-lending.html" class="dropdown-item">Lending</a>
                                                <a href="crypto-orders.html" class="dropdown-item">Orders</a>
                                                <a href="crypto-kyc-application.html" class="dropdown-item">KYC
                                                    Application</a>
                                                <a href="crypto-ico-landing.html" class="dropdown-item">ICO Landing</a>
                                            </div>
                                        </div>

                                        <div class="dropdown">
                                            <a class="dropdown-item dropdown-toggle arrow-none" href="#" id="topnav-project" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Projects <div class="arrow-down"></div>
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="topnav-project">
                                                <a href="projects-grid.html" class="dropdown-item">Projects Grid</a>
                                                <a href="projects-list.html" class="dropdown-item">Projects List</a>
                                                <a href="projects-overview.html" class="dropdown-item">Project Overview</a>
                                                <a href="projects-create.html" class="dropdown-item">Create New</a>
                                            </div>
                                        </div>
                                        <div class="dropdown">
                                            <a class="dropdown-item dropdown-toggle arrow-none" href="#" id="topnav-task" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Tasks <div class="arrow-down"></div>
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="topnav-task">
                                                <a href="tasks-list.html" class="dropdown-item">Task List</a>
                                                <a href="tasks-kanban.html" class="dropdown-item">Kanban Board</a>
                                                <a href="tasks-create.html" class="dropdown-item">Create Task</a>
                                            </div>
                                        </div>
                                        <div class="dropdown">
                                            <a class="dropdown-item dropdown-toggle arrow-none" href="#" id="topnav-contact" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Contacts <div class="arrow-down"></div>
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="topnav-contact">
                                                <a href="contacts-grid.html" class="dropdown-item">User Grid</a>
                                                <a href="contacts-list.html" class="dropdown-item">User List</a>
                                                <a href="contacts-profile.html" class="dropdown-item">Profile</a>
                                            </div>
                                        </div>
                                        <div class="dropdown">
                                            <a class="dropdown-item dropdown-toggle arrow-none" href="#" id="topnav-blog" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span>Blog</span>
                                                <div class="arrow-down"></div>
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="topnav-blog">
                                                <a href="blog-list.html" class="dropdown-item">Blog List</a>
                                                <a href="blog-grid.html" class="dropdown-item">Blog Grid</a>
                                                <a href="blog-details.html" class="dropdown-item">Blog Details</a>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-components" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="bx bx-collection mr-2"></i>Components <div class="arrow-down"></div>
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="topnav-components">
                                        <div class="dropdown">
                                            <a class="dropdown-item dropdown-toggle arrow-none" href="#" id="topnav-form" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Forms <div class="arrow-down"></div>
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="topnav-form">
                                                <a href="form-elements.html" class="dropdown-item">Form Elements</a>
                                                <a href="form-layouts.html" class="dropdown-item">Form Layouts</a>
                                                <a href="form-validation.html" class="dropdown-item">Form Validation</a>
                                                <a href="form-advanced.html" class="dropdown-item">Form Advanced</a>
                                                <a href="form-editors.html" class="dropdown-item">Form Editors</a>
                                                <a href="form-uploads.html" class="dropdown-item">Form File Upload</a>
                                                <a href="form-xeditable.html" class="dropdown-item">Form Xeditable</a>
                                                <a href="form-repeater.html" class="dropdown-item">Form Repeater</a>
                                                <a href="form-wizard.html" class="dropdown-item">Form Wizard</a>
                                                <a href="form-mask.html" class="dropdown-item">Form Mask</a>
                                            </div>
                                        </div>
                                        <div class="dropdown">
                                            <a class="dropdown-item dropdown-toggle arrow-none" href="#" id="topnav-table" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Tables <div class="arrow-down"></div>
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="topnav-table">
                                                <a href="tables-basic.html" class="dropdown-item">Basic Tables</a>
                                                <a href="tables-datatable.html" class="dropdown-item">Data Tables</a>
                                                <a href="tables-responsive.html" class="dropdown-item">Responsive Table</a>
                                                <a href="tables-editable.html" class="dropdown-item">Editable Table</a>
                                            </div>
                                        </div>
                                        <div class="dropdown">
                                            <a class="dropdown-item dropdown-toggle arrow-none" href="#" id="topnav-charts" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Charts <div class="arrow-down"></div>
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="topnav-charts">
                                                <a href="charts-apex.html" class="dropdown-item">Apex charts</a>
                                                <a href="charts-echart.html" class="dropdown-item">E charts</a>
                                                <a href="charts-chartjs.html" class="dropdown-item">Chartjs Chart</a>
                                                <a href="charts-flot.html" class="dropdown-item">Flot Chart</a>
                                                <a href="charts-tui.html" class="dropdown-item">Toast UI Chart</a>
                                                <a href="charts-knob.html" class="dropdown-item">Jquery Knob Chart</a>
                                                <a href="charts-sparkline.html" class="dropdown-item">Sparkline Chart</a>
                                            </div>
                                        </div>
                                        <div class="dropdown">
                                            <a class="dropdown-item dropdown-toggle arrow-none" href="#" id="topnav-icons" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Icons <div class="arrow-down"></div>
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="topnav-icons">
                                                <a href="icons-boxicons.html" class="dropdown-item">Boxicons</a>
                                                <a href="icons-materialdesign.html" class="dropdown-item">Material
                                                    Design</a>
                                                <a href="icons-dripicons.html" class="dropdown-item">Dripicons</a>
                                                <a href="icons-fontawesome.html" class="dropdown-item">Font awesome</a>
                                            </div>
                                        </div>
                                        <div class="dropdown">
                                            <a class="dropdown-item dropdown-toggle arrow-none" href="#" id="topnav-map" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Maps <div class="arrow-down"></div>
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="topnav-map">
                                                <a href="maps-google.html" class="dropdown-item">Google Maps</a>
                                                <a href="maps-vector.html" class="dropdown-item">Vector Maps</a>
                                                <a href="maps-leaflet.html" class="dropdown-item">Leaflet Maps</a>
                                            </div>
                                        </div>
                                    </div>
                                </li>



                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-more" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="bx bx-file mr-2"></i>Extra pages <div class="arrow-down"></div>
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="topnav-more">
                                        <div class="dropdown">
                                            <a class="dropdown-item dropdown-toggle arrow-none" href="#" id="topnav-invoice" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Invoices <div class="arrow-down"></div>
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="topnav-invoice">
                                                <a href="invoices-list.html" class="dropdown-item">Invoice List</a>
                                                <a href="invoices-detail.html" class="dropdown-item">Invoice Detail</a>
                                            </div>
                                        </div>
                                        <div class="dropdown">
                                            <a class="dropdown-item dropdown-toggle arrow-none" href="#" id="topnav-auth" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Authentication <div class="arrow-down"></div>
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="topnav-auth">
                                                <a target="_blank" href="auth-login.html" class="dropdown-item">Login</a>
                                                <a target="_blank" href="auth-login-2.html" class="dropdown-item">Login</a>
                                                <a target="_blank" href="auth-register.html" class="dropdown-item">Register</a>
                                                <a target="_blank" href="auth-register-2.html" class="dropdown-item">Register</a>
                                                <a target="_blank" href="auth-recoverpw.html" class="dropdown-item">Recover
                                                    Password</a>
                                                <a target="_blank" href="auth-recoverpw-2.html" class="dropdown-item">Recover
                                                    Password</a>
                                                <a target="_blank" href="auth-lock-screen.html" class="dropdown-item">Lock
                                                    Screen</a>
                                                <a target="_blank" href="auth-lock-screen-2.html" class="dropdown-item">Lock
                                                    Screen</a>
                                                <a target="_blank" href="auth-confirm-mail.html" class="dropdown-item">Confirm Mail</a>
                                                <a target="_blank" href="auth-confirm-mail-2.html" class="dropdown-item">Confirm Mail 2</a>
                                                <a target="_blank" href="auth-email-verification.html" class="dropdown-item">Email verification</a>
                                                <a target="_blank" href="auth-email-verification-2.html" class="dropdown-item">Email verification 2</a>
                                                <a target="_blank" href="auth-two-step-verification.html" class="dropdown-item">Two step verification</a>
                                                <a target="_blank" href="auth-two-step-verification-2.html" class="dropdown-item">Two step verification 2</a>
                                            </div>
                                        </div>
                                        <div class="dropdown">
                                            <a class="dropdown-item dropdown-toggle arrow-none" href="#" id="topnav-utility" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Utility <div class="arrow-down"></div>
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="topnav-utility">
                                                <a href="pages-starter.html" class="dropdown-item">Starter Page</a>
                                                <a target="_blank" href="pages-maintenance.html" class="dropdown-item">Maintenance</a>
                                                <a target="_blank" href="pages-comingsoon.html" class="dropdown-item">Coming
                                                    Soon</a>
                                                <a href="pages-timeline.html" class="dropdown-item">Timeline</a>
                                                <a href="pages-faqs.html" class="dropdown-item">FAQs</a>
                                                <a href="pages-pricing.html" class="dropdown-item">Pricing</a>
                                                <a target="_blank" href="pages-404.html" class="dropdown-item">Error 404</a>
                                                <a target="_blank" href="pages-500.html" class="dropdown-item">Error 500</a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item dropdown active">
                                    <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-layout" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="bx bx-layout mr-2"></i>Layouts <div class="arrow-down"></div>
                                    </a>
                                    <div class="dropdown-menu active" aria-labelledby="topnav-layout">
                                        <div class="dropdown">
                                            <a class="dropdown-item dropdown-toggle arrow-none" href="#" id="topnav-layout-verti" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span>Vertical</span> <div class="arrow-down"></div>
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="topnav-layout-verti">
                                                <a target="_blank" href="layouts-light-sidebar.html" class="dropdown-item">Light Sidebar</a>
                                                <a target="_blank" href="layouts-compact-sidebar.html" class="dropdown-item">Compact Sidebar</a>
                                                <a target="_blank" href="layouts-icon-sidebar.html" class="dropdown-item">Icon Sidebar</a>
                                                <a target="_blank" href="layouts-boxed.html" class="dropdown-item">Boxed Width</a>
                                                <a target="_blank" href="layouts-preloader.html" class="dropdown-item">Preloader</a>
                                                <a target="_blank" href="layouts-colored-sidebar.html" class="dropdown-item">Colored Sidebar</a>
                                                <a target="_blank" href="layouts-scrollable.html" class="dropdown-item">Scrollable</a>
                                            </div>
                                        </div>
            
                                        <div class="dropdown active">
                                            <a class="dropdown-item dropdown-toggle arrow-none" href="#" id="topnav-layout-hori" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span>Horizontal</span> <div class="arrow-down"></div>
                                            </a>
                                            <div class="dropdown-menu active" aria-labelledby="topnav-layout-hori">
                                                <a target="_blank" href="layouts-horizontal.html" class="dropdown-item">Horizontal</a>
                                                <a target="_blank" href="layouts-h-topbar-light.html" class="dropdown-item">Topbar light</a>
                                                <a target="_blank" href="layouts-h-boxed.html" class="dropdown-item active">Boxed width</a>
                                                <a target="_blank" href="layouts-h-preloader.html" class="dropdown-item">Preloader</a>
                                                <a target="_blank" href="layouts-h-colored.html" class="dropdown-item">Colored Header</a>
                                                <a target="_blank" href="layouts-h-scrollable.html" class="dropdown-item">Scrollable</a>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </nav>
                </div>
            </div> -->

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">
                <div class="page-content">
                    <?= $content; ?>
                </div>
                <!-- End Page-content -->


            <!-- end main content-->

            </div>
        <!-- END layout-wrapper -->
        </div>


        


    </body>
</html>