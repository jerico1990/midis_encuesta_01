<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="container-fluid">

    <?php if($snip && $encuesta==0){ ?>
    <?php $form = ActiveForm::begin(['options' => ['id' => 'formEncuesta01']]); ?>
        <!-- start page title -->
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <h1 class="card-title mt-0">Ficha de determinación de Tambo Bicentenario</h1>
                        <p class="text-danger mb-0">*Obligatorio</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card overflow-hidden">
                    <div class="card-body bg-soft-primary">
                        <h5 class="card-title text-black mb-0">Datos generales</h5>
                    </div>
                    <div class="card-body text-justify">
                        <div class="table-responsive">
                            <table class="table table-nowrap mb-0">
                                <tbody>
                                    <tr>
                                        <th scope="row">Nombre - Tambo :</th>
                                        <td> <span class="TAMBO"></span> </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Departamento - Tambo :</th>
                                        <td> <span class="DEPARTAMENTO"></span> </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Provincia - Tambo :</th>
                                        <td> <span class="PROVINCIA"></span> </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Distrito - Tambo :</th>
                                        <td> <span class="DISTRITO"></span> </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">DNI del evaluador :</th>
                                        <td> <span class="DNI"></span> </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Apellidos y nombres del evaluador :</th>
                                        <td> <span class="GESTOR"></span> </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card overflow-hidden">
                    <div class="card-body bg-soft-primary">
                        <h5 class="card-title text-black mb-0">Aspectos para evaluar para la implementación de la estrategia de Tambo Bicentenario en el marco de la Política Social y Hambre Cero</h5>
                    </div>
                    <div class="card-body text-justify">
                        <label>
                            1. ¿Las vías y medios de transporte son accesibles para llegar al tambo, considerando el tiempo y distancia? <span class="text-danger">*</span> <br>
                            <small>Este componente está referido a la ubicación de los tambos en relación con la población del área de influencia, cuanto más cercano esté la población al tambo, se prevé una operatividad eficiente</small>
                        </label>
                        <div class="form-inline error_p01">
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                                <input type="radio" id="p01_1" name="Encuesta01[p01]" value="1" class="custom-control-input" data-parsley-group="group_p01">
                                <label class="custom-control-label" for="p01_1">Totalmente</label>
                            </div>
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                                <input type="radio" id="p01_2" name="Encuesta01[p01]" value="2" class="custom-control-input" data-parsley-group="group_p01">
                                <label class="custom-control-label" for="p01_2">Parcialmente</label>
                            </div>
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                                <input type="radio" id="p01_3" name="Encuesta01[p01]" value="3" class="custom-control-input" data-parsley-group="group_p01">
                                <label class="custom-control-label" for="p01_3">Ninguno o nunca</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card overflow-hidden">
                    <div class="card-body text-justify">
                        <label>
                            2. ¿Cuenta con servicio de Agua? <span class="text-danger">*</span> <br>
                            <small>Este criterio apunta a que el Tambo cuenta con el servicio de agua operativo.</small>
                        </label>
                        <div class="form-inline error_p02">
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                                <input type="radio" id="p02_1" name="Encuesta01[p02]" value="1" class="custom-control-input">
                                <label class="custom-control-label" for="p02_1">Totalmente</label>
                            </div>
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                                <input type="radio" id="p02_2" name="Encuesta01[p02]" value="2" class="custom-control-input">
                                <label class="custom-control-label" for="p02_2">Parcialmente</label>
                            </div>
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                                <input type="radio" id="p02_3" name="Encuesta01[p02]" value="3" class="custom-control-input">
                                <label class="custom-control-label" for="p02_3">Ninguno o nunca</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card overflow-hidden">
                    <div class="card-body text-justify">
                        <label>
                            3. ¿Cuenta con servicio de Internet? <span class="text-danger">*</span> <br>
                            <small>Este criterio indica si el tambo cuenta con Internet y si este servicio, es permanente.</small>
                        </label>
                        <div class="form-inline error_p03">
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                                <input type="radio" id="p03_1" name="Encuesta01[p03]" value="1" class="custom-control-input">
                                <label class="custom-control-label" for="p03_1">Totalmente</label>
                            </div>
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                                <input type="radio" id="p03_2" name="Encuesta01[p03]" value="2" class="custom-control-input">
                                <label class="custom-control-label" for="p03_2">Parcialmente</label>
                            </div>
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                                <input type="radio" id="p03_3" name="Encuesta01[p03]" value="3" class="custom-control-input">
                                <label class="custom-control-label" for="p03_3">Ninguno o nunca</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row p03" style="display:none">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card overflow-hidden">
                    <div class="card-body text-justify">
                        <label>
                            3. 1.¿Cuál es la velocidad de internet? (Para realizar la medición del internte ingrese al siguiente enlace <a href="http://fast.com">http://fast.com</a> ) <span class="text-danger">*</span> <br>
                        </label>
                        <input type="text" class="form-control error_p03_velocidad" name="Encuesta01[p03_velocidad]" id="p03_velocidad">
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card overflow-hidden">
                    <div class="card-body text-justify">
                        <label>
                            4. ¿Cuenta con servicio de telefonía (móvil o fija)? <span class="text-danger">*</span> <br>
                            <small>Este criterio indica si el tambo cuenta con el servicio de funcionando.</small>
                        </label>
                        <div class="form-inline error_p04">
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                                <input type="radio" id="p04_1" name="Encuesta01[p04]" value="1" class="custom-control-input">
                                <label class="custom-control-label" for="p04_1">Totalmente</label>
                            </div>
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                                <input type="radio" id="p04_2" name="Encuesta01[p04]" value="2" class="custom-control-input">
                                <label class="custom-control-label" for="p04_2">Parcialmente</label>
                            </div>
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                                <input type="radio" id="p04_3" name="Encuesta01[p04]" value="3" class="custom-control-input">
                                <label class="custom-control-label" for="p04_3">Ninguno o nunca</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card overflow-hidden">
                    <div class="card-body text-justify">
                        <label>
                            5. ¿Cuenta con Energía Eléctrica? <span class="text-danger">*</span> <br>
                            <small>Este criterio indica si el tambo cuenta con energía eléctrica, suministro </small>
                        </label>
                        <div class="form-inline error_p05">
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                                <input type="radio" id="p05_1" name="Encuesta01[p05]" value="1" class="custom-control-input">
                                <label class="custom-control-label" for="p05_1">Totalmente</label>
                            </div>
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                                <input type="radio" id="p05_2" name="Encuesta01[p05]" value="2" class="custom-control-input">
                                <label class="custom-control-label" for="p05_2">Parcialmente</label>
                            </div>
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                                <input type="radio" id="p05_3" name="Encuesta01[p05]" value="3" class="custom-control-input">
                                <label class="custom-control-label" for="p05_3">Ninguno o nunca</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card overflow-hidden">
                    <div class="card-body text-justify">
                        <label>
                            6. ¿Servicio de Guardianía? <span class="text-danger">*</span> <br>
                            <small>Este criterio indica si el tambo cuenta con el servicio de guardianía como complemento a la estrategia de TB</small>
                        </label>
                        <div class="form-inline error_p06">
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                                <input type="radio" id="p06_1" name="Encuesta01[p06]" value="1" class="custom-control-input">
                                <label class="custom-control-label" for="p06_1">Totalmente</label>
                            </div>
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                                <input type="radio" id="p06_2" name="Encuesta01[p06]" value="2" class="custom-control-input">
                                <label class="custom-control-label" for="p06_2">Parcialmente</label>
                            </div>
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                                <input type="radio" id="p06_3" name="Encuesta01[p06]" value="3" class="custom-control-input">
                                <label class="custom-control-label" for="p06_3">Ninguno o nunca</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card overflow-hidden">
                    <div class="card-body text-justify">
                        <label>
                            7. ¿Los ambientes (oficina, dormitorio, cocina,zum) están operativos? <span class="text-danger">*</span> <br>
                            <small>Este criterio indica si los ambientes del Tambo, están operativos.</small>
                        </label>
                        <div class="form-inline error_p07">
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                                <input type="radio" id="p07_1" name="Encuesta01[p07]" value="1" class="custom-control-input">
                                <label class="custom-control-label" for="p07_1">Totalmente</label>
                            </div>
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                                <input type="radio" id="p07_2" name="Encuesta01[p07]" value="2" class="custom-control-input">
                                <label class="custom-control-label" for="p07_2">Parcialmente</label>
                            </div>
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                                <input type="radio" id="p07_3" name="Encuesta01[p07]" value="3" class="custom-control-input">
                                <label class="custom-control-label" for="p07_3">Ninguno o nunca</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card overflow-hidden">
                    <div class="card-body text-justify">
                        <label>
                            8. ¿El Tambo cuenta con acceso a agua de riego? <span class="text-danger">*</span> <br>
                            <small>Este criterio apunta a que el Tambo cuenta con acceso a agua de riego, por cualquier tecnología, (Inundación, Aspersión, Goteo) importante para desarrollar las parcelas productivas.</small>
                        </label>
                        <div class="form-inline error_p08">
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                                <input type="radio" id="p08_1" name="Encuesta01[p08]" value="1" class="custom-control-input">
                                <label class="custom-control-label" for="p08_1">Totalmente</label>
                            </div>
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                                <input type="radio" id="p08_2" name="Encuesta01[p08]" value="2" class="custom-control-input">
                                <label class="custom-control-label" for="p08_2">Parcialmente</label>
                            </div>
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                                <input type="radio" id="p08_3" name="Encuesta01[p08]" value="3" class="custom-control-input">
                                <label class="custom-control-label" for="p08_3">Ninguno o nunca</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card overflow-hidden">
                    <div class="card-body text-justify">
                        <label>
                            9. En el último año, ¿Usted identifica  presencia del Gobierno Regional en ámbito de Tambo? <span class="text-danger">*</span> <br>
                            <!-- <small>Este criterio indica si la Instancia de Articulación Regional (IAR) participará de la estrategia de Tambo Bicentenario dentro del marco de sus funciones.</small> -->
                        </label>
                        <div class="form-inline error_p09">
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                                <input type="radio" id="p09_1" name="Encuesta01[p09]" value="1" class="custom-control-input">
                                <label class="custom-control-label" for="p09_1">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                                <input type="radio" id="p09_2" name="Encuesta01[p09]" value="2" class="custom-control-input">
                                <label class="custom-control-label" for="p09_2">No</label>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card overflow-hidden">
                    <div class="card-body text-justify">
                        <label>
                            10. En el último año, ¿Identifica presencia de instituciones u otros que desarrollen labores en el ámbito productivo, en el ámbito de influencia del Tambo? <span class="text-danger">*</span> <br>
                            <!-- <small>Este criterio indica si en el área de influencia del tambo, se encuentran diversas instituciones con el objetivo de coadyuvar a la implementación de los módulos productivos; pueden ver más de una.</small> -->
                        </label>
                        <div class="form-inline error_p10">
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                                <input type="radio" id="p10_1" name="Encuesta01[p10]" value="1" class="custom-control-input">
                                <label class="custom-control-label" for="p10_1">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                                <input type="radio" id="p10_2" name="Encuesta01[p10]" value="2" class="custom-control-input">
                                <label class="custom-control-label" for="p10_2">No</label>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row p10_lista" style="display:none">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card overflow-hidden">
                    <div class="card-body text-justify">
                        <label>
                            10. 1. Lista de Entidades (Seleccione una o varias alternativas) <br>
                        </label>
                        <div class="error_p10_lista">
                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p10_01_1" name="Encuesta01[p10_01_1]">
                                <label class="custom-control-label" for="p10_01_1">MIDAGRI</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p10_01_2" name="Encuesta01[p10_01_2]">
                                <label class="custom-control-label" for="p10_01_2">AGROBANCO</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p10_01_3" name="Encuesta01[p10_01_3]">
                                <label class="custom-control-label" for="p10_01_3">AGROIDEAS</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p10_01_4" name="Encuesta01[p10_01_4]">
                                <label class="custom-control-label" for="p10_01_4">AGRORURAL</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p10_01_5" name="Encuesta01[p10_01_5]">
                                <label class="custom-control-label" for="p10_01_5">SENASA</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p10_01_6" name="Encuesta01[p10_01_6]">
                                <label class="custom-control-label" for="p10_01_6">INIA</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p10_01_7" name="Encuesta01[p10_01_7]">
                                <label class="custom-control-label" for="p10_01_7">PRODUCE</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p10_01_8" name="Encuesta01[p10_01_8]">
                                <label class="custom-control-label" for="p10_01_8">MINCETUR</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p10_01_9" name="Encuesta01[p10_01_9]">
                                <label class="custom-control-label" for="p10_01_9">GERENCIA REGIONAL DE DESARROLLO ECONÓMICO </label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p10_01_10" name="Encuesta01[p10_01_10]">
                                <label class="custom-control-label" for="p10_01_10">DIRECCIÓN REGIONAL DE AGRICULTURA </label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p10_01_11" name="Encuesta01[p10_01_11]">
                                <label class="custom-control-label" for="p10_01_11">AGENCIA AGRARIA </label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p10_01_12" name="Encuesta01[p10_01_12]">
                                <label class="custom-control-label" for="p10_01_12">GOBIERNO LOCAL</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p10_01_13" name="Encuesta01[p10_01_13]">
                                <label class="custom-control-label" for="p10_01_13">ODEL</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p10_01_14" name="Encuesta01[p10_01_14]">
                                <label class="custom-control-label" for="p10_01_14">ONG</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p10_01_15" name="Encuesta01[p10_01_15]">
                                <label class="custom-control-label" for="p10_01_15">OTROS</label>
                            </div>
                        </div>
                        <div class="p10_01_15" style="display:none">
                            <input type="text" class="form-control error_p10_01_15" name="Encuesta01[p10_01_15_otros]" id="p10_01_15_otros">
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card overflow-hidden">
                    <div class="card-body text-justify">
                        <label>
                            11. En el último año, ¿La Instancia de Articulación Local (IAL) ha planteado estrategias para asegurar el cumplimiento de la Meta 4: Acciones para promover la adecuada alimentación, y la prevención y reducción de la anemia? <span class="text-danger">*</span> <br>
                            <!-- <small>Este indicador permite vincular a la instancia de articulación local a través de su Programa de Incentivos para la gestión Municipal en temas de anemia, transversal al módulo DIT.</small> -->
                        </label>
                        <div class="form-inline error_p11">
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                                <input type="radio" id="p11_1" name="Encuesta01[p11]" value="1" class="custom-control-input">
                                <label class="custom-control-label" for="p11_1">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                                <input type="radio" id="p11_2" name="Encuesta01[p11]" value="2" class="custom-control-input">
                                <label class="custom-control-label" for="p11_2">No</label>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card overflow-hidden">
                    <div class="card-body text-justify">
                        <label>
                            12. En el último año, ¿Usted identifico la presencia de instituciones o programas vinculados al desarrollo infantil temprano en el ámbito de influencia del Tambo? <span class="text-danger">*</span> <br>
                            <small>Este criterio indica si en el área de influencia del tambo, se encuentran instituciones; Programas, proyectos y actividades vinculadas al Desarrollo Infantil Temprano; con el objetivo de coadyuvar en la implementación del SAF.</small>
                        </label>
                        <div class="form-inline error_p12">
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                                <input type="radio" id="p12_1" name="Encuesta01[p12]" value="1" class="custom-control-input">
                                <label class="custom-control-label" for="p12_1">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                                <input type="radio" id="p12_2" name="Encuesta01[p12]" value="2" class="custom-control-input">
                                <label class="custom-control-label" for="p12_2">No</label>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


        <div class="row p12_lista" style="display:none">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card overflow-hidden">
                    <div class="card-body text-justify">
                        <label>
                            12. 1. Listado de instituciones vinculadas al Desarrollo Infantil Temprano (Seleccione una o varias alternativas) <br>
                        </label>
                        <div class="error_p12_lista">
                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p12_01_1" name="Encuesta01[p12_01_1]">
                                <label class="custom-control-label" for="p12_01_1">PN Cuna Mas</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p12_01_2" name="Encuesta01[p12_01_2]">
                                <label class="custom-control-label" for="p12_01_2">PN Juntos</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p12_01_3" name="Encuesta01[p12_01_3]">
                                <label class="custom-control-label" for="p12_01_3">Ministerio de la Mujer</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p12_01_4" name="Encuesta01[p12_01_4]">
                                <label class="custom-control-label" for="p12_01_4">Minsa</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p12_01_5" name="Encuesta01[p12_01_5]">
                                <label class="custom-control-label" for="p12_01_5">MINEDU</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p12_01_6" name="Encuesta01[p12_01_6]">
                                <label class="custom-control-label" for="p12_01_6">MVCS</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p12_01_7" name="Encuesta01[p12_01_7]">
                                <label class="custom-control-label" for="p12_01_7">RENIEC</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p12_01_8" name="Encuesta01[p12_01_8]">
                                <label class="custom-control-label" for="p12_01_8">Gobierno Regional/gerencia de desarrollo social</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p12_01_9" name="Encuesta01[p12_01_9]">
                                <label class="custom-control-label" for="p12_01_9">DIRESA /GERESA</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p12_01_10" name="Encuesta01[p12_01_10]">
                                <label class="custom-control-label" for="p12_01_10">Dirección Regional de Educación</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p12_01_11" name="Encuesta01[p12_01_11]">
                                <label class="custom-control-label" for="p12_01_11">GOBIERNO LOCAL</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p12_01_12" name="Encuesta01[p12_01_12]">
                                <label class="custom-control-label" for="p12_01_12">ONG</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p12_01_13" name="Encuesta01[p12_01_13]">
                                <label class="custom-control-label" for="p12_01_13">OTROS</label>
                            </div>
                        </div>
                        <div class="p12_01_13 " style="display:none">
                            <input type="text" class="form-control error_p12_01_13" name="Encuesta01[p12_01_13_otros]" id="p12_01_13_otros">
                        </div>
                        

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card overflow-hidden">
                    <div class="card-body text-justify">
                        <label>
                            13. En el último año, ¿Usted identificó la presencia de Organismos No Gubernamentales (ONG) realizando actividades en del ámbito de influencia de los Tambos? <span class="text-danger">*</span> <br>
                            <!-- <small>Este indicador permite identificar a ONGs dentro del ámbito de influencia de los tambos para integrarlos en actividades vinculantes a la estrategia de Tambo Bicentenario, tales como Organismos No Gubernamentales en último año. </small> -->
                        </label>
                        <div class="form-inline error_p13">
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                                <input type="radio" id="p13_1" name="Encuesta01[p13]" value="1" class="custom-control-input">
                                <label class="custom-control-label" for="p13_1">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                                <input type="radio" id="p13_2" name="Encuesta01[p13]" value="2" class="custom-control-input">
                                <label class="custom-control-label" for="p13_2">No</label>
                            </div>
                        </div>
                        <div class="mt-3 p13" style="display:none">
                            <label>ONG:</label>
                            <input type="text" class="form-control error_p13_ong" name="Encuesta01[p13_ong]" id="p13_ong">
                            <label>Tema:</label>
                            <input type="text" class="form-control error_p13_tema" name="Encuesta01[p13_tema]" id="p13_tema">
                        </div>
                        

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card overflow-hidden">
                    <div class="card-body text-justify">
                        <label>
                            14. En el último año, ¿Usted identificó la presencia  empresas privada, dentro del ámbito de influencia de los Tambos?  <span class="text-danger">*</span> <br>
                            <small>Este indicador permite identificar a otros actores del sector privada dentro del ámbito de influencia de los tambos para integrarlos en actividades vinculantes a la estrategia de Tambo Bicentenario, empresas privadas como mineras y otras.</small>
                        </label>
                        <div class="form-inline error_p14">
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                                <input type="radio" id="p14_1" name="Encuesta01[p14]" value="1" class="custom-control-input">
                                <label class="custom-control-label" for="p14_1">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                                <input type="radio" id="p14_2" name="Encuesta01[p14]" value="2" class="custom-control-input">
                                <label class="custom-control-label" for="p14_2">No</label>
                            </div>
                        </div>
                        <div class="mt-3 p14" style="display:none">
                            <label>Empresa:</label>
                            <input type="text" class="form-control error_p14_empresa" name="Encuesta01[p14_empresa]" id="p14_empresa">
                        </div>

                        <!-- <div class="mt-3 p14" style="display:none">
                            <label>Componente:</label>
                            <select name="Encuesta01[p14_componente]" id="p14_componente" class="form-control error_p14_componente">
                                <option value>Seleccionar</option>
                                <option value="MODULO DE DESARROLLO INFANTIL TEMPRANO">MÓDULO DE DESARROLLO INFANTIL TEMPRANO</option>
                                <option value="CENTRO DE INNOVACION">CENTRO DE INNOVACION</option>
                                <option value="MÓDULO DE DESARROLLO PRODUCTIVO E INCLUSIÓN FINANCIERA HAMBRE CERO">MÓDULO DE DESARROLLO PRODUCTIVO E INCLUSIÓN FINANCIERA HAMBRE CERO</option>
                                <option value="GESTORES CON COMPETENCIAS">GESTORES CON COMPETENCIAS</option>
                            </select>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="row p14_lista" style="display:none">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card overflow-hidden">
                    <div class="card-body text-justify">
                        <label>
                            14. 1. Listado de componentes (Seleccione una o varias alternativas) <br>
                        </label>
                        <div class="error_p14_lista">
                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p14_01_1" name="Encuesta01[p14_01_1]">
                                <label class="custom-control-label" for="p14_01_1">MÓDULO DE DESARROLLO INFANTIL TEMPRANO</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p14_01_2" name="Encuesta01[p14_01_2]">
                                <label class="custom-control-label" for="p14_01_2">CENTRO DE INNOVACION</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p14_01_3" name="Encuesta01[p14_01_3]">
                                <label class="custom-control-label" for="p14_01_3">MÓDULO DE DESARROLLO PRODUCTIVO E INCLUSIÓN FINANCIERA HAMBRE CERO</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p14_01_4" name="Encuesta01[p14_01_4]">
                                <label class="custom-control-label" for="p14_01_4">GESTORES CON COMPETENCIAS</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card overflow-hidden">
                    <div class="card-body text-justify">
                        <label>
                            15.	En el último año, ¿Usted identifico la presencia a Universidad públicas o privadas en el ámbito de influencia del Tambo para integrarlos en actividades vinculantes al Tambo? <span class="text-danger">*</span> <br>
                            <!-- <small>Este indicador permite identificar a otros actores dentro del ámbito de influencia de los tambos para integrarlos en actividades vinculantes a la estrategia de Tambo Bicentenario, como son las universidades públicas_____ o Universidades privadas____ en el marco de la responsabilidad social universitaria.</small> -->
                        </label>
                        <div class="form-inline error_p15">
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                                <input type="radio" id="p15_1" name="Encuesta01[p15]" value="1" class="custom-control-input">
                                <label class="custom-control-label" for="p15_1">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                                <input type="radio" id="p15_2" name="Encuesta01[p15]" value="2" class="custom-control-input">
                                <label class="custom-control-label" for="p15_2">No</label>
                            </div>
                        </div>
                        <div class="mt-3 p15" style="display:none">
                            <label>Universidad:</label>
                            <input type="text" class="form-control error_p15_universidad" name="Encuesta01[p15_universidad]" id="p15_universidad">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card overflow-hidden">
                    <div class="card-body text-justify">
                        <label>
                            16.	En el último año, ¿Usted identificó la presencia de otros ministerios o instituciones públicas tales como (MINEDU, MINSA, MINCUL, MIMP., MINJUS, MINTRA) desarrollando actividades en el ámbito de influencia del Tambo? <span class="text-danger">*</span> <br>
                            <!-- <small>Este indicador permite identificar otros ministerios en actividades vinculantes a la estrategia de Tambo Bicentenario, tales como (MINEDU, MINSA, MINCUL, MIMP., MINJUS, MINTRA). </small> -->
                        </label>
                        <div class="form-inline error_p16">
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                                <input type="radio" id="p16_1" name="Encuesta01[p16]" value="1" class="custom-control-input">
                                <label class="custom-control-label" for="p16_1">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                                <input type="radio" id="p16_2" name="Encuesta01[p16]" value="2" class="custom-control-input">
                                <label class="custom-control-label" for="p16_2">No</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row p16_lista" style="display:none">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card overflow-hidden">
                    <div class="card-body text-justify">
                        <label>
                            16. 1. Listado de ministerios y/o instituciones públicas que desarrollan actividades en el ámbito de influencia del TAMBO. (Seleccione una o varias alternativas) <br>
                        </label>
                        <div class="error_p16_lista">
                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p16_01_1" name="Encuesta01[p16_01_1]">
                                <label class="custom-control-label" for="p16_01_1">MINEDU</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p16_01_2" name="Encuesta01[p16_01_2]">
                                <label class="custom-control-label" for="p16_01_2">MINSA</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p16_01_3" name="Encuesta01[p16_01_3]">
                                <label class="custom-control-label" for="p16_01_3">MINCUL</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p16_01_4" name="Encuesta01[p16_01_4]">
                                <label class="custom-control-label" for="p16_01_4">MIMP</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p16_01_5" name="Encuesta01[p16_01_5]">
                                <label class="custom-control-label" for="p16_01_5">MINJUS</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p16_01_6" name="Encuesta01[p16_01_6]">
                                <label class="custom-control-label" for="p16_01_6">MINTRA</label>
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="p16_01_7" name="Encuesta01[p16_01_7]">
                                <label class="custom-control-label" for="p16_01_7">OTROS</label>
                            </div>
                        </div>
                        <div class="p16_01_7" style="display:none">
                            <input type="text" class="form-control error_p16_01_7" name="Encuesta01[p16_01_7_otros]" id="p16_01_7_otros">
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card overflow-hidden">
                    <div class="card-body text-justify">
                        <label>
                        17. Comentarios y observaciones.
                        </label>
                        <textarea class="form-control" name="Encuesta01[p17]" id="p17" rows="5"></textarea>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <button type="button" class="btn btn-primary mb-3 btn-enviar-encuesta">Enviar</button>
            </div>
        </div>
    <?php ActiveForm::end(); ?>

    <?php } elseif($encuesta == 1) { ?>
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6 text-center">
                <h1 class="card-title mt-0">Su encuesta ya se encuentra registrada</h1> 
            </div>
        </div>

    <?php } else { ?>
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6 text-center">
                <h1 class="card-title mt-0">Página no encontrada - Parámetro invalido</h1> 
            </div>
        </div>

    <?php } ?>
</div>
<!-- container-fluid -->
<script>

// const message = JSON.stringify({
//     message: 'Hello from iframe',
//     date: Date.now(),
// });
// window.parent.postMessage(message, '*');


var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var snip = "<?= $snip ?>";
var encuesta = "<?= $encuesta ?>";

if(encuesta == "1"){
    const message = JSON.stringify({
        success: true,
    });
    window.parent.postMessage(message, '*');
}
/* Evento de la Pregunta 03 */
$('body').on('click', '#p03_1', function (e) {
    $('.p03').show();
});

$('body').on('click', '#p03_2', function (e) {
    $('.p03').show();
});

$('body').on('click', '#p03_3', function (e) {
    $('.p03').hide();
    $('#p03_velocidad').val('');
});



$('body').on('click', '#p10_1', function (e) {
    $('.p10_lista').show();
});

$('body').on('click', '#p10_01_15', function (e) {
    var p10_01_15 = $("[name=\"Encuesta01[p10_01_15]\"]:checked").val();
    if(p10_01_15=='on'){
        $('.p10_01_15').show();
    }else{
        $('.p10_01_15').hide();
        $('#p10_01_15_otros').val('');
    }
});



$('body').on('click', '#p10_2', function (e) {
    $('.p10_lista').hide();
    $("#p10_01_1").prop('checked', false);
    $("#p10_01_2").prop('checked', false);
    $("#p10_01_3").prop('checked', false);
    $("#p10_01_4").prop('checked', false);
    $("#p10_01_5").prop('checked', false);
    $("#p10_01_6").prop('checked', false);
    $("#p10_01_7").prop('checked', false);
    $("#p10_01_8").prop('checked', false);
    $("#p10_01_9").prop('checked', false);
    $("#p10_01_10").prop('checked', false);
    $("#p10_01_11").prop('checked', false);
    $("#p10_01_12").prop('checked', false);
    $("#p10_01_13").prop('checked', false);
    $("#p10_01_14").prop('checked', false);
    $("#p10_01_15").prop('checked', false);
    $("#p10_01_15_otros").val('');
});



$('body').on('click', '#p12_1', function (e) {
    $('.p12_lista').show();
});

$('body').on('click', '#p12_01_13', function (e) {
    var p12_01_13 = $("[name=\"Encuesta01[p12_01_13]\"]:checked").val();
    if(p12_01_13=='on'){
        $('.p12_01_13').show();
    }else{
        $('.p12_01_13').hide();
        $('#p12_01_13_otros').val('');
    }
});



$('body').on('click', '#p12_2', function (e) {
    $('.p12_lista').hide();
    $("#p12_01_1").prop('checked', false);
    $("#p12_01_2").prop('checked', false);
    $("#p12_01_3").prop('checked', false);
    $("#p12_01_4").prop('checked', false);
    $("#p12_01_5").prop('checked', false);
    $("#p12_01_6").prop('checked', false);
    $("#p12_01_7").prop('checked', false);
    $("#p12_01_8").prop('checked', false);
    $("#p12_01_9").prop('checked', false);
    $("#p12_01_10").prop('checked', false);
    $("#p12_01_11").prop('checked', false);
    $("#p12_01_12").prop('checked', false);
    $("#p12_01_13").prop('checked', false);
    $("#p12_01_13_otros").val('');
});


$('body').on('click', '#p13_1', function (e) {
    $('.p13').show();
});

$('body').on('click', '#p13_2', function (e) {
    $('.p13').hide();
    $("#p13_ong").val('');
    $("#p13_tema").val('');
});

$('body').on('click', '#p14_1', function (e) {
    $('.p14_lista').show();
    $('.p14').show();
});

$('body').on('click', '#p14_2', function (e) {
    $('.p14_lista').hide();
    $('.p14').hide();
    $("#p14_01_1").prop('checked', false);
    $("#p14_01_2").prop('checked', false);
    $("#p14_01_3").prop('checked', false);
    $("#p14_01_4").prop('checked', false);
    $("#p14_empresa").val('');
});


$('body').on('click', '#p15_1', function (e) {
    $('.p15').show();
});

$('body').on('click', '#p15_2', function (e) {
    $('.p15').hide();
    $("#p15_universidad").val('');
});


$('body').on('click', '#p16_1', function (e) {
    $('.p16_lista').show();
});

$('body').on('click', '#p16_01_7', function (e) {
    var p16_01_7 = $("[name=\"Encuesta01[p16_01_7]\"]:checked").val();
    if(p16_01_7=='on'){
        $('.p16_01_7').show();
    }else{
        $('.p16_01_7').hide();
        $('#p16_01_7_otros').val('');
    }
});



$('body').on('click', '#p16_2', function (e) {
    $('.p16_lista').hide();
    $("#p16_01_1").prop('checked', false);
    $("#p16_01_2").prop('checked', false);
    $("#p16_01_3").prop('checked', false);
    $("#p16_01_4").prop('checked', false);
    $("#p16_01_5").prop('checked', false);
    $("#p16_01_6").prop('checked', false);
    $("#p16_01_7").prop('checked', false);
    $("#p16_01_7_otros").val('');
});
// $('body').on('click', '#p10_1', function (e) {
//     $('.p10_01').show();
// });

// $('body').on('click', '#p10_2', function (e) {
//     $('.p10_01').hide();
// });

$('body').on('click', '.btn-enviar-encuesta', function (e) {
    //e.preventDefault();
    var form = $('#formEncuesta01');
    var error           = "";
    var p01             = $("[name=\"Encuesta01[p01]\"]:checked").val();
    var p02             = $("[name=\"Encuesta01[p02]\"]:checked").val();
    var p03             = $("[name=\"Encuesta01[p03]\"]:checked").val();
    var p03_velocidad   = $("[name=\"Encuesta01[p03_velocidad]\"]").val();
    var p04             = $("[name=\"Encuesta01[p04]\"]:checked").val();
    var p05             = $("[name=\"Encuesta01[p05]\"]:checked").val();
    var p06             = $("[name=\"Encuesta01[p06]\"]:checked").val();
    var p07             = $("[name=\"Encuesta01[p07]\"]:checked").val();
    var p08             = $("[name=\"Encuesta01[p08]\"]:checked").val();
    var p09             = $("[name=\"Encuesta01[p09]\"]:checked").val();
    var p10             = $("[name=\"Encuesta01[p10]\"]:checked").val();

    var p10_01_1        = ($("[name=\"Encuesta01[p10_01_1]\"]:checked").val())?1:0;
    var p10_01_2        = ($("[name=\"Encuesta01[p10_01_2]\"]:checked").val())?1:0;
    var p10_01_3        = ($("[name=\"Encuesta01[p10_01_3]\"]:checked").val())?1:0;
    var p10_01_4        = ($("[name=\"Encuesta01[p10_01_4]\"]:checked").val())?1:0;
    var p10_01_5        = ($("[name=\"Encuesta01[p10_01_5]\"]:checked").val())?1:0;
    var p10_01_6        = ($("[name=\"Encuesta01[p10_01_6]\"]:checked").val())?1:0;
    var p10_01_7        = ($("[name=\"Encuesta01[p10_01_7]\"]:checked").val())?1:0;
    var p10_01_8        = ($("[name=\"Encuesta01[p10_01_8]\"]:checked").val())?1:0;
    var p10_01_9        = ($("[name=\"Encuesta01[p10_01_9]\"]:checked").val())?1:0;
    var p10_01_10       = ($("[name=\"Encuesta01[p10_01_10]\"]:checked").val())?1:0;
    var p10_01_11       = ($("[name=\"Encuesta01[p10_01_11]\"]:checked").val())?1:0;
    var p10_01_12       = ($("[name=\"Encuesta01[p10_01_12]\"]:checked").val())?1:0;
    var p10_01_13       = ($("[name=\"Encuesta01[p10_01_13]\"]:checked").val())?1:0;
    var p10_01_14       = ($("[name=\"Encuesta01[p10_01_14]\"]:checked").val())?1:0;
    var p10_01_15       = ($("[name=\"Encuesta01[p10_01_15]\"]:checked").val())?1:0;
    var p10_01_15_otros = $("[name=\"Encuesta01[p10_01_15_otros]\"]").val();

    var p10_contador    = p10_01_1 + p10_01_2 + p10_01_3 + p10_01_4 + p10_01_5 + p10_01_6 + p10_01_7 + p10_01_8 + p10_01_9 + p10_01_10 + p10_01_11 + p10_01_12 + p10_01_13 + p10_01_14 + p10_01_15;


    var p11             = $("[name=\"Encuesta01[p11]\"]:checked").val();
    var p12             = $("[name=\"Encuesta01[p12]\"]:checked").val();

    var p12_01_1        = ($("[name=\"Encuesta01[p12_01_1]\"]:checked").val())?1:0;
    var p12_01_2        = ($("[name=\"Encuesta01[p12_01_2]\"]:checked").val())?1:0;
    var p12_01_3        = ($("[name=\"Encuesta01[p12_01_3]\"]:checked").val())?1:0;
    var p12_01_4        = ($("[name=\"Encuesta01[p12_01_4]\"]:checked").val())?1:0;
    var p12_01_5        = ($("[name=\"Encuesta01[p12_01_5]\"]:checked").val())?1:0;
    var p12_01_6        = ($("[name=\"Encuesta01[p12_01_6]\"]:checked").val())?1:0;
    var p12_01_7        = ($("[name=\"Encuesta01[p12_01_7]\"]:checked").val())?1:0;
    var p12_01_8        = ($("[name=\"Encuesta01[p12_01_8]\"]:checked").val())?1:0;
    var p12_01_9        = ($("[name=\"Encuesta01[p12_01_9]\"]:checked").val())?1:0;
    var p12_01_10       = ($("[name=\"Encuesta01[p12_01_10]\"]:checked").val())?1:0;
    var p12_01_11       = ($("[name=\"Encuesta01[p12_01_11]\"]:checked").val())?1:0;
    var p12_01_12       = ($("[name=\"Encuesta01[p12_01_12]\"]:checked").val())?1:0;
    var p12_01_13       = ($("[name=\"Encuesta01[p12_01_13]\"]:checked").val())?1:0;
    var p12_01_13_otros = $("[name=\"Encuesta01[p12_01_13_otros]\"]").val();
    var p12_contador    = p12_01_1 + p12_01_2 + p12_01_3 + p12_01_4 + p12_01_5 + p12_01_6 + p12_01_7 + p12_01_8 + p12_01_9 + p12_01_10 + p12_01_11 + p12_01_12 + p12_01_13;

    var p13             = $("[name=\"Encuesta01[p13]\"]:checked").val();
    var p13_ong         = $("[name=\"Encuesta01[p13_ong]\"]").val();
    var p13_tema        = $("[name=\"Encuesta01[p13_tema]\"]").val();

    var p14             = $("[name=\"Encuesta01[p14]\"]:checked").val();
    var p14_empresa     = $("[name=\"Encuesta01[p14_empresa]\"]").val();
    var p14_01_1        = ($("[name=\"Encuesta01[p14_01_1]\"]:checked").val())?1:0;
    var p14_01_2        = ($("[name=\"Encuesta01[p14_01_2]\"]:checked").val())?1:0;
    var p14_01_3        = ($("[name=\"Encuesta01[p14_01_3]\"]:checked").val())?1:0;
    var p14_01_4        = ($("[name=\"Encuesta01[p14_01_4]\"]:checked").val())?1:0;
    var p14_contador    = p14_01_1 + p14_01_2 + p14_01_3 + p14_01_4;

    // var p14_componente  = $("[name=\"Encuesta01[p14_componente]\"]").val();

    var p15             = $("[name=\"Encuesta01[p15]\"]:checked").val();
    var p15_universidad = $("[name=\"Encuesta01[p15_universidad]\"]").val();

    var p16             = $("[name=\"Encuesta01[p16]\"]:checked").val();

    var p16_01_1        = ($("[name=\"Encuesta01[p16_01_1]\"]:checked").val())?1:0;
    var p16_01_2        = ($("[name=\"Encuesta01[p16_01_2]\"]:checked").val())?1:0;
    var p16_01_3        = ($("[name=\"Encuesta01[p16_01_3]\"]:checked").val())?1:0;
    var p16_01_4        = ($("[name=\"Encuesta01[p16_01_4]\"]:checked").val())?1:0;
    var p16_01_5        = ($("[name=\"Encuesta01[p16_01_5]\"]:checked").val())?1:0;
    var p16_01_6        = ($("[name=\"Encuesta01[p16_01_6]\"]:checked").val())?1:0;
    var p16_01_7        = ($("[name=\"Encuesta01[p16_01_7]\"]:checked").val())?1:0;
    var p16_01_7_otros  = $("[name=\"Encuesta01[p16_01_7_otros]\"]").val();
    var p16_contador    = p16_01_1 + p16_01_2 + p16_01_3 + p16_01_4 + p16_01_5 + p16_01_6 + p16_01_7;
    var p17             = $("[name=\"Encuesta01[p17]\"]").val();
    //formData.push({name: 'username', value: 'this is username'});
    
    if(!p01){
        $('.error_p01').addClass('alert-danger');
        $("#p01_1").focus();
        error = error + "error 1";
    }else{
        $('.error_p01').removeClass('alert-danger');
    }

    if(!p02){
        $('.error_p02').addClass('alert-danger');
        $("#p02_1").focus();
        error = error + "error 2";
    }else{
        $('.error_p02').removeClass('alert-danger');
    }

    if(!p03){
        $('.error_p03').addClass('alert-danger');
        $("#p03_1").focus();
        error = error + "error 3";
    }else{
        $('.error_p03').removeClass('alert-danger');
    }

    if(p03 && (p03=="1" || p03=="2") && !p03_velocidad){
        $('.error_p03_velocidad').addClass('alert-danger');
        $("#p03_velocidad").focus();
        error = error + "error 3 1";
    }else{
        $('#error_p03_velocidad').removeClass('alert-danger');
    }

    if(!p04){
        $('.error_p04').addClass('alert-danger');
        $("#p04").focus();
        error = error + "error 4";
    }else{
        $('.error_p04').removeClass('alert-danger');
    }

    if(!p05){
        $('.error_p05').addClass('alert-danger');
        $("#p05").focus();
        error = error + "error 5";
    }else{
        $('.error_p05').removeClass('alert-danger');
    }

    if(!p06){
        $('.error_p06').addClass('alert-danger');
        $("#p06").focus();
        error = error + "error 6";
    }else{
        $('.error_p06').removeClass('alert-danger');
    }

    if(!p07){
        $('.error_p07').addClass('alert-danger');
        $("#p07").focus();
        error = error + "error 7";
    }else{
        $('.error_p07').removeClass('alert-danger');
    }

    if(!p08){
        $('.error_p08').addClass('alert-danger');
        $("#p08").focus();
        error = error + "error 8";
    }else{
        $('.error_p08').removeClass('alert-danger');
    }

    if(!p09){
        $('.error_p09').addClass('alert-danger');
        $("#p09").focus();
        error = error + "error 9";
    }else{
        $('.error_p09').removeClass('alert-danger');
    }

    if(!p10){
        $('.error_p10').addClass('alert-danger');
        $("#p10").focus();
        error = error + "error 10";
    }else{
        $('.error_p10').removeClass('alert-danger');
    }

    if(p10 && p10=="1" && p10_contador == 0){
        $('.error_p10_lista').addClass('alert-danger');
        $("#p10_01_1").focus();
        error = error + "error lista";
    }else{
        $('.error_p10_lista').removeClass('alert-danger');
    }

    if(p10 && p10=="1" && p10_01_15 == 1 && !p10_01_15_otros){
        $('.error_p10_01_15').addClass('alert-danger');
        $("#p10_01_15_otros").focus();
        error = error + "error otros p10";
    }else{
        $('.error_p10_01_15').removeClass('alert-danger');
    }

    if(!p11){
        $('.error_p11').addClass('alert-danger');
        $("#p11").focus();
        error = error + "error p11";
    }else{
        $('.error_p11').removeClass('alert-danger');
    }

    if(!p12){
        $('.error_p12').addClass('alert-danger');
        $("#p12").focus();
        error = error + "error p12";
    }else{
        $('.error_p12').removeClass('alert-danger');
    }
    
    if(p12 && p12=="1" && p12_contador == 0){
        $('.error_p12_lista').addClass('alert-danger');
        $("#p12_01_1").focus();
        error = error + "error p12_lista";
    }else{
        $('.error_p12_lista').removeClass('alert-danger');
    }

    if(p12 && p12=="1" && p12_01_13 == 1 && !p12_01_13_otros){
        $('.error_p12_01_13').addClass('alert-danger');
        $("#p12_01_13_otros").focus();
        error = error + "error p12_13";
    }else{
        $('.error_p12_01_13').removeClass('alert-danger');
    }

    if(!p13){
        $('.error_p13').addClass('alert-danger');
        $("#p13").focus();
        error = error + "error p13";
    }else{
        $('.error_p13').removeClass('alert-danger');
    }

    if(p13 && p13=="1" && p13_ong==""){
        $('.error_p13_ong').addClass('alert-danger');
        $("#p13_ong").focus();
        error = error + "error p13_ong";
    }else{
        $('.error_p13_ong').removeClass('alert-danger');
    }

    if(p13 && p13=="1" && p13_tema==""){
        $('.error_p13_tema').addClass('alert-danger');
        $("#p13_tema").focus();
        error = error + "error p13_tema";
    }else{
        $('.error_p13_tema').removeClass('alert-danger');
    }


    if(!p14){
        $('.error_p14').addClass('alert-danger');
        $("#p14").focus();
        error = error + "error p14";
    }else{
        $('.error_p14').removeClass('alert-danger');
    }

    if(p14 && p14=="1" && p14_contador == 0){
        $('.error_p14_lista').addClass('alert-danger');
        $("#p14_01_1").focus();
        error = error + "error p14_lista";
    }else{
        $('.error_p14_lista').removeClass('alert-danger');
    }

    if(p14 && p14=="1" && p14_empresa==""){
        $('.error_p14_empresa').addClass('alert-danger');
        $("#p14_empresa").focus();
        error = error + "error p14_empresa";
    }else{
        $('.error_p14_empresa').removeClass('alert-danger');
    }

    // if(p14 && p14=="1" && p14_componente==""){
    //     $('.error_p14_componente').addClass('alert-danger');
    //     error = error + "error p14_componente";
    // }else{
    //     $('.error_p14_componente').removeClass('alert-danger');
    // }

    if(!p15){
        $('.error_p15').addClass('alert-danger');
        $("#p15").focus();
        error = error + "error p15";
    }else{
        $('.error_p15').removeClass('alert-danger');
    }

    if(p15 && p15=="1" && p15_universidad==""){
        $('.error_p15_universidad').addClass('alert-danger');
        $("#p15_universidad").focus();
        error = error + "error p15_universidad";
    }else{
        $('.error_p15_universidad').removeClass('alert-danger');
    }

    if(!p16){
        $('.error_p16').addClass('alert-danger');
        $("#p16").focus();
        error = error + "error p16";
    }else{
        $('.error_p16').removeClass('alert-danger');
    }
    
    if(p16 && p16=="1" && p16_contador == 0){
        $('.error_p16_lista').addClass('alert-danger');
        $("#p16_01_1").focus();
        error = error + "error p16_lista";
    }else{
        $('.error_p16_lista').removeClass('alert-danger');
    }

    if(p16 && p16=="1" && p16_01_7 == 1 && !p16_01_7_otros){
        $('.error_p16_01_7').addClass('alert-danger');
        $("#p16_01_7_otros").focus();
        error = error + "error p16_7";
    }else{
        $('.error_p16_01_7').removeClass('alert-danger');
    }
    
    if(error != ""){
        console.log(error);
        return false;
    }
    $(this).prop('disabled', true);
    
    var form_data = new FormData(); // $('#formEncuesta01').serializeArray();
    form_data.append("_csrf", csrf);
    form_data.append("Encuesta01[snip]", snip);
    form_data.append("Encuesta01[p01]", p01);
    form_data.append("Encuesta01[p02]", p02);
    form_data.append("Encuesta01[p03]", p03);
    form_data.append("Encuesta01[p03_velocidad]", p03_velocidad);
    form_data.append("Encuesta01[p04]", p04);
    form_data.append("Encuesta01[p05]", p05);
    form_data.append("Encuesta01[p06]", p06);
    form_data.append("Encuesta01[p07]", p07);
    form_data.append("Encuesta01[p08]", p08);
    form_data.append("Encuesta01[p09]", p09);
    form_data.append("Encuesta01[p10]", p10);
    form_data.append("Encuesta01[p10_01_1]", p10_01_1);
    form_data.append("Encuesta01[p10_01_2]", p10_01_2);
    form_data.append("Encuesta01[p10_01_3]", p10_01_3);
    form_data.append("Encuesta01[p10_01_4]", p10_01_4);
    form_data.append("Encuesta01[p10_01_5]", p10_01_5);
    form_data.append("Encuesta01[p10_01_6]", p10_01_6);
    form_data.append("Encuesta01[p10_01_7]", p10_01_7);
    form_data.append("Encuesta01[p10_01_8]", p10_01_8);
    form_data.append("Encuesta01[p10_01_9]", p10_01_9);
    form_data.append("Encuesta01[p10_01_10]", p10_01_10);
    form_data.append("Encuesta01[p10_01_11]", p10_01_11);
    form_data.append("Encuesta01[p10_01_12]", p10_01_12);
    form_data.append("Encuesta01[p10_01_13]", p10_01_13);
    form_data.append("Encuesta01[p10_01_14]", p10_01_14);
    form_data.append("Encuesta01[p10_01_15]", p10_01_15);
    form_data.append("Encuesta01[p10_01_15_otros]", p10_01_15_otros);

    form_data.append("Encuesta01[p11]", p11);
    form_data.append("Encuesta01[p12]", p12);

    form_data.append("Encuesta01[p12_01_1]", p12_01_1);
    form_data.append("Encuesta01[p12_01_2]", p12_01_2);
    form_data.append("Encuesta01[p12_01_3]", p12_01_3);
    form_data.append("Encuesta01[p12_01_4]", p12_01_4);
    form_data.append("Encuesta01[p12_01_5]", p12_01_5);
    form_data.append("Encuesta01[p12_01_6]", p12_01_6);
    form_data.append("Encuesta01[p12_01_7]", p12_01_7);
    form_data.append("Encuesta01[p12_01_8]", p12_01_8);
    form_data.append("Encuesta01[p12_01_9]", p12_01_9);
    form_data.append("Encuesta01[p12_01_10]", p12_01_10);
    form_data.append("Encuesta01[p12_01_11]", p12_01_11);
    form_data.append("Encuesta01[p12_01_12]", p12_01_12);
    form_data.append("Encuesta01[p12_01_13]", p12_01_13);
    form_data.append("Encuesta01[p12_01_13_otros]", p12_01_13_otros);

    form_data.append("Encuesta01[p13]", p13);
    form_data.append("Encuesta01[p13_ong]", p13_ong);
    form_data.append("Encuesta01[p13_tema]", p13_tema);

    form_data.append("Encuesta01[p14]", p14);
    form_data.append("Encuesta01[p14_empresa]", p14_empresa);
    form_data.append("Encuesta01[p14_01_1]", p14_01_1);
    form_data.append("Encuesta01[p14_01_2]", p14_01_2);
    form_data.append("Encuesta01[p14_01_3]", p14_01_3);
    form_data.append("Encuesta01[p14_01_4]", p14_01_4);
    // form_data.append("Encuesta01[p14_componente]", p14_componente);

    form_data.append("Encuesta01[p15]", p15);
    form_data.append("Encuesta01[p15_universidad]", p15_universidad);

    form_data.append("Encuesta01[p16]", p16);
    form_data.append("Encuesta01[p16_01_1]", p16_01_1);
    form_data.append("Encuesta01[p16_01_2]", p16_01_2);
    form_data.append("Encuesta01[p16_01_3]", p16_01_3);
    form_data.append("Encuesta01[p16_01_4]", p16_01_4);
    form_data.append("Encuesta01[p16_01_5]", p16_01_5);
    form_data.append("Encuesta01[p16_01_6]", p16_01_6);
    form_data.append("Encuesta01[p16_01_7]", p16_01_7);
    form_data.append("Encuesta01[p16_01_7_otros]", p16_01_7_otros);

    form_data.append("Encuesta01[p17]", p17);


    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        cache: false,
		contentType: false,
		processData: false,
        data: form_data,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        success: function (results) {
            if(results.success){
                // const message = JSON.stringify({
                //     success: true,
                // });
                // window.parent.postMessage(message, '*');

                //window.top.postMessage('hello', '*')
                //results.success;
                location.reload();
            }
        },
    });
});


DatosGenerales()
async function DatosGenerales(){

    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/encuesta/datos-generales',
        method: 'POST',
        data:{"_csrf":csrf,"snip":snip},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {
            if(results.success){
                $('.TAMBO').html(results.data.TAMBO);
                $('.DEPARTAMENTO').html(results.data.DEPARTAMENTO);
                $('.PROVINCIA').html(results.data.PROVINCIA);
                $('.DISTRITO').html(results.data.DISTRITO);
                $('.GESTOR').html(results.data.GESTOR);
                $('.DNI').html(results.data.DNI);
            }
        },
        error:function(){
            alert('No hay conectividad con el sistema');
        }
    });
}
</script>