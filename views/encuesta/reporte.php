<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h1 class="card-title mt-0">Instrumento para la valoración del Tambo Bicentenario - características del Tambo y su Ámbito de Influencia</h1>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card overflow-hidden">
                <div class="card-body bg-soft-primary">
                    <h5 class="card-title text-black mb-0">Reporte</h5>
                </div>
                <div class="card-body text-justify">
                    <div class="table-responsive">
                        <table class="table table-nowrap mb-0">
                            <thead>
                                <th scope="row">Nombre - Tambo :</th>
                                <th scope="row">Departamento - Tambo :</th>
                                <th scope="row">Provincia - Tambo :</th>
                                <th scope="row">Distrito - Tambo :</th>
                                <th scope="row">DNI del evaluador:</th>
                                <th scope="row">Apellidos y nombres del evaluador:</th>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- container-fluid -->
<script>

// const message = JSON.stringify({
//     message: 'Hello from iframe',
//     date: Date.now(),
// });
// window.parent.postMessage(message, '*');


var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";


async function DatosGeneralesEncuestas(){

    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/encuesta/datos-generales-encuestas',
        method: 'POST',
        data:{"_csrf":csrf},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {
            if(results.success){
                
            }
        },
        error:function(){
            alert('No hay conectividad con el sistema');
        }
    });
}
</script>