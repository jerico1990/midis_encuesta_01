<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <h1 class="card-title mt-0">Ficha de determinación de Tambo Bicentenario</h1>
                    <p class="text-danger mb-0">*Obligatorio</p>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="card overflow-hidden">
                <div class="card-body bg-soft-primary">
                    <h5 class="card-title text-black mb-0">Datos generales</h5>
                </div>
                <div class="card-body text-justify">
                    <div class="table-responsive">
                        <table class="table table-nowrap mb-0">
                            <tbody>
                                <tr>
                                    <th scope="row">Nombre - Tambo :</th>
                                    <td> <span class="TAMBO"></span> </td>
                                </tr>
                                <tr>
                                    <th scope="row">Departamento - Tambo :</th>
                                    <td> <span class="DEPARTAMENTO"></span> </td>
                                </tr>
                                <tr>
                                    <th scope="row">Provincia - Tambo :</th>
                                    <td> <span class="PROVINCIA"></span> </td>
                                </tr>
                                <tr>
                                    <th scope="row">Distrito - Tambo :</th>
                                    <td> <span class="DISTRITO"></span> </td>
                                </tr>
                                <tr>
                                    <th scope="row">DNI del evaluador:</th>
                                    <td> <span class="DNI"></span> </td>
                                </tr>
                                <tr>
                                    <th scope="row">Apellidos y nombres del evaluador:</th>
                                    <td> <span class="GESTOR"></span> </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="card overflow-hidden">
                <div class="card-body bg-soft-primary">
                    <h5 class="card-title text-black mb-0">Aspectos para evaluar para la implementación de la estrategia de Tambo Bicentenario en el marco de la Política Social y Hambre Cero</h5>
                </div>
                <div class="card-body text-justify">
                    <label>
                        1. ¿Las vías y medios de transporte son accesibles para llegar al tambo, considerando el tiempo y distancia? <span class="text-danger">*</span> <br>
                        <small>Este componente está referido a la ubicación de los tambos en relación con la población del área de influencia, cuanto más cercano esté la población al tambo, se prevé una operatividad eficiente</small>
                    </label>
                    <div class="form-inline error_p01 ">
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                            <input type="radio" id="p01_1" name="Encuesta01[p01]" value="1" class="custom-control-input" disabled data-parsley-group="group_p01">
                            <label class="custom-control-label" for="p01_1">Totalmente</label>
                        </div>
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                            <input type="radio" id="p01_2" name="Encuesta01[p01]" value="2" class="custom-control-input" disabled data-parsley-group="group_p01">
                            <label class="custom-control-label" for="p01_2">Parcialmente</label>
                        </div>
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                            <input type="radio" id="p01_3" name="Encuesta01[p01]" value="3" class="custom-control-input" disabled data-parsley-group="group_p01">
                            <label class="custom-control-label" for="p01_3">Ninguno o nunca</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="card overflow-hidden">
                <div class="card-body text-justify">
                    <label>
                        2. ¿Cuenta con servicio de Agua? <span class="text-danger">*</span> <br>
                        <small>Este criterio apunta a que el Tambo cuenta con el servicio de agua operativo.</small>
                    </label>
                    <div class="form-inline error_p02">
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                            <input type="radio" id="p02_1" name="Encuesta01[p02]" value="1" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p02_1">Totalmente</label>
                        </div>
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                            <input type="radio" id="p02_2" name="Encuesta01[p02]" value="2" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p02_2">Parcialmente</label>
                        </div>
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                            <input type="radio" id="p02_3" name="Encuesta01[p02]" value="3" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p02_3">Ninguno o nunca</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="card overflow-hidden">
                <div class="card-body text-justify">
                    <label>
                        3. ¿Cuenta con servicio de Internet? <span class="text-danger">*</span> <br>
                        <small>Este criterio indica si el tambo cuenta con Internet y si este servicio, es permanente.</small>
                    </label>
                    <div class="form-inline error_p03">
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                            <input type="radio" id="p03_1" name="Encuesta01[p03]" value="1" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p03_1">Totalmente</label>
                        </div>
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                            <input type="radio" id="p03_2" name="Encuesta01[p03]" value="2" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p03_2">Parcialmente</label>
                        </div>
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                            <input type="radio" id="p03_3" name="Encuesta01[p03]" value="3" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p03_3">Ninguno o nunca</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row p03" style="display:none">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="card overflow-hidden">
                <div class="card-body text-justify">
                    <label>
                        3. 1.¿Cuál es la velocidad de internet? (Para realizar la medición del internte ingrese al siguiente enlace <a href="http://fast.com">http://fast.com</a> ) <span class="text-danger">*</span> <br>
                    </label>
                    <input type="text" class="form-control error_p03_velocidad" name="Encuesta01[p03_velocidad]" disabled id="p03_velocidad">
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="card overflow-hidden">
                <div class="card-body text-justify">
                    <label>
                        4. ¿Cuenta con servicio de telefonía (móvil o fija)? <span class="text-danger">*</span> <br>
                        <small>Este criterio indica si el tambo cuenta con el servicio de funcionando.</small>
                    </label>
                    <div class="form-inline error_p04">
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                            <input type="radio" id="p04_1" name="Encuesta01[p04]" value="1" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p04_1">Totalmente</label>
                        </div>
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                            <input type="radio" id="p04_2" name="Encuesta01[p04]" value="2" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p04_2">Parcialmente</label>
                        </div>
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                            <input type="radio" id="p04_3" name="Encuesta01[p04]" value="3" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p04_3">Ninguno o nunca</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="card overflow-hidden">
                <div class="card-body text-justify">
                    <label>
                        5. ¿Cuenta con Energía Eléctrica? <span class="text-danger">*</span> <br>
                        <small>Este criterio indica si el tambo cuenta con energía eléctrica, suministro </small>
                    </label>
                    <div class="form-inline error_p05">
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                            <input type="radio" id="p05_1" name="Encuesta01[p05]" value="1" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p05_1">Totalmente</label>
                        </div>
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                            <input type="radio" id="p05_2" name="Encuesta01[p05]" value="2" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p05_2">Parcialmente</label>
                        </div>
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                            <input type="radio" id="p05_3" name="Encuesta01[p05]" value="3" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p05_3">Ninguno o nunca</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="card overflow-hidden">
                <div class="card-body text-justify">
                    <label>
                        6. ¿Servicio de Guardianía? <span class="text-danger">*</span> <br>
                        <small>Este criterio indica si el tambo cuenta con el servicio de guardianía como complemento a la estrategia de TB</small>
                    </label>
                    <div class="form-inline error_p06">
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                            <input type="radio" id="p06_1" name="Encuesta01[p06]" value="1" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p06_1">Totalmente</label>
                        </div>
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                            <input type="radio" id="p06_2" name="Encuesta01[p06]" value="2" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p06_2">Parcialmente</label>
                        </div>
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                            <input type="radio" id="p06_3" name="Encuesta01[p06]" value="3" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p06_3">Ninguno o nunca</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="card overflow-hidden">
                <div class="card-body text-justify">
                    <label>
                        7. ¿Los ambientes (oficina, dormitorio, cocina,zum) están operativos? <span class="text-danger">*</span> <br>
                        <small>Este criterio indica si los ambientes del Tambo, están operativos.</small>
                    </label>
                    <div class="form-inline error_p07">
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                            <input type="radio" id="p07_1" name="Encuesta01[p07]" value="1" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p07_1">Totalmente</label>
                        </div>
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                            <input type="radio" id="p07_2" name="Encuesta01[p07]" value="2" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p07_2">Parcialmente</label>
                        </div>
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                            <input type="radio" id="p07_3" name="Encuesta01[p07]" value="3" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p07_3">Ninguno o nunca</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="card overflow-hidden">
                <div class="card-body text-justify">
                    <label>
                        8. ¿El Tambo cuenta con acceso a agua de riego? <span class="text-danger">*</span> <br>
                        <small>Este criterio apunta a que el Tambo cuenta con acceso a agua de riego, por cualquier tecnología, (Inundación, Aspersión, Goteo) importante para desarrollar las parcelas productivas.</small>
                    </label>
                    <div class="form-inline error_p08">
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                            <input type="radio" id="p08_1" name="Encuesta01[p08]" value="1" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p08_1">Totalmente</label>
                        </div>
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                            <input type="radio" id="p08_2" name="Encuesta01[p08]" value="2" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p08_2">Parcialmente</label>
                        </div>
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary col">
                            <input type="radio" id="p08_3" name="Encuesta01[p08]" value="3" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p08_3">Ninguno o nunca</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="card overflow-hidden">
                <div class="card-body text-justify">
                    <label>
                        9. En el último año, ¿Usted identifica  presencia del Gobierno Regional en ámbito de Tambo? <span class="text-danger">*</span> <br>
                        <!-- <small>Este criterio indica si la Instancia de Articulación Regional (IAR) participará de la estrategia de Tambo Bicentenario dentro del marco de sus funciones.</small> -->
                    </label>
                    <div class="form-inline error_p09">
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                            <input type="radio" id="p09_1" name="Encuesta01[p09]" value="1" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p09_1">Si</label>
                        </div>
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                            <input type="radio" id="p09_2" name="Encuesta01[p09]" value="2" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p09_2">No</label>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="card overflow-hidden">
                <div class="card-body text-justify">
                    <label>
                        10. En el último año, ¿Identifica presencia de instituciones u otros que desarrollen labores en el ámbito productivo, en el ámbito de influencia del Tambo? <span class="text-danger">*</span> <br>
                        <!-- <small>Este criterio indica si en el área de influencia del tambo, se encuentran diversas instituciones con el objetivo de coadyuvar a la implementación de los módulos productivos; pueden ver más de una.</small> -->
                    </label>
                    <div class="form-inline error_p10">
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                            <input type="radio" id="p10_1" name="Encuesta01[p10]" value="1" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p10_1">Si</label>
                        </div>
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                            <input type="radio" id="p10_2" name="Encuesta01[p10]" value="2" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p10_2">No</label>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row p10_lista" style="display:none">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="card overflow-hidden">
                <div class="card-body text-justify">
                    <label>
                        10. 1. Lista de Entidades (Seleccione una o varias alternativas) <br>
                    </label>
                    <div class="error_p10_lista">
                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p10_01_1" disabled name="Encuesta01[p10_01_1]">
                            <label class="custom-control-label" for="p10_01_1">MIDAGRI</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p10_01_2" disabled name="Encuesta01[p10_01_2]">
                            <label class="custom-control-label" for="p10_01_2">AGROBANCO</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p10_01_3" disabled name="Encuesta01[p10_01_3]">
                            <label class="custom-control-label" for="p10_01_3">AGROIDEAS</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p10_01_4" disabled name="Encuesta01[p10_01_4]">
                            <label class="custom-control-label" for="p10_01_4">AGRORURAL</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p10_01_5" disabled name="Encuesta01[p10_01_5]">
                            <label class="custom-control-label" for="p10_01_5">SENASA</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p10_01_6" disabled name="Encuesta01[p10_01_6]">
                            <label class="custom-control-label" for="p10_01_6">INIA</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p10_01_7" disabled name="Encuesta01[p10_01_7]">
                            <label class="custom-control-label" for="p10_01_7">PRODUCE</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p10_01_8" disabled name="Encuesta01[p10_01_8]">
                            <label class="custom-control-label" for="p10_01_8">MINCETUR</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p10_01_9" disabled name="Encuesta01[p10_01_9]">
                            <label class="custom-control-label" for="p10_01_9">GERENCIA REGIONAL DE DESARROLLO ECONÓMICO </label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p10_01_10" disabled name="Encuesta01[p10_01_10]">
                            <label class="custom-control-label" for="p10_01_10">DIRECCIÓN REGIONAL DE AGRICULTURA </label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p10_01_11" disabled name="Encuesta01[p10_01_11]">
                            <label class="custom-control-label" for="p10_01_11">AGENCIA AGRARIA </label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p10_01_12" disabled name="Encuesta01[p10_01_12]">
                            <label class="custom-control-label" for="p10_01_12">GOBIERNO LOCAL</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p10_01_13" disabled name="Encuesta01[p10_01_13]">
                            <label class="custom-control-label" for="p10_01_13">ODEL</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p10_01_14" disabled name="Encuesta01[p10_01_14]">
                            <label class="custom-control-label" for="p10_01_14">ONG</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p10_01_15" disabled name="Encuesta01[p10_01_15]">
                            <label class="custom-control-label" for="p10_01_15">OTROS</label>
                        </div>
                    </div>
                    <div class="p10_01_15" style="display:none">
                        <input type="text" class="form-control error_p10_01_15" name="Encuesta01[p10_01_15_otros]" disabled id="p10_01_15_otros">
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="card overflow-hidden">
                <div class="card-body text-justify">
                    <label>
                        11. En el último año, ¿La Instancia de Articulación Local (IAL) ha planteado estrategias para asegurar el cumplimiento de la Meta 4: Acciones para promover la adecuada alimentación, y la prevención y reducción de la anemia? <span class="text-danger">*</span> <br>
                        <!-- <small>Este indicador permite vincular a la instancia de articulación local a través de su Programa de Incentivos para la gestión Municipal en temas de anemia, transversal al módulo DIT.</small> -->
                    </label>
                    <div class="form-inline error_p11">
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                            <input type="radio" id="p11_1" name="Encuesta01[p11]" value="1" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p11_1">Si</label>
                        </div>
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                            <input type="radio" id="p11_2" name="Encuesta01[p11]" value="2" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p11_2">No</label>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="card overflow-hidden">
                <div class="card-body text-justify">
                    <label>
                        12. En el último año, ¿Usted identifico la presencia de instituciones o programas vinculados al desarrollo infantil temprano en el ámbito de influencia del Tambo? <span class="text-danger">*</span> <br>
                        <small>Este criterio indica si en el área de influencia del tambo, se encuentran instituciones; Programas, proyectos y actividades vinculadas al Desarrollo Infantil Temprano; con el objetivo de coadyuvar en la implementación del SAF.</small>
                    </label>
                    <div class="form-inline error_p12">
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                            <input type="radio" id="p12_1" name="Encuesta01[p12]" value="1" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p12_1">Si</label>
                        </div>
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                            <input type="radio" id="p12_2" name="Encuesta01[p12]" value="2" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p12_2">No</label>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <div class="row p12_lista" style="display:none">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="card overflow-hidden">
                <div class="card-body text-justify">
                    <label>
                        12. 1. Listado de instituciones vinculadas al Desarrollo Infantil Temprano (Seleccione una o varias alternativas) <br>
                    </label>
                    <div class="error_p12_lista">
                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p12_01_1" disabled name="Encuesta01[p12_01_1]">
                            <label class="custom-control-label" for="p12_01_1">PN Cuna Mas</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p12_01_2" disabled name="Encuesta01[p12_01_2]">
                            <label class="custom-control-label" for="p12_01_2">PN Juntos</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p12_01_3" disabled name="Encuesta01[p12_01_3]">
                            <label class="custom-control-label" for="p12_01_3">Ministerio de la Mujer</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p12_01_4" disabled name="Encuesta01[p12_01_4]">
                            <label class="custom-control-label" for="p12_01_4">Minsa</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p12_01_5" disabled name="Encuesta01[p12_01_5]">
                            <label class="custom-control-label" for="p12_01_5">MINEDU</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p12_01_6" disabled name="Encuesta01[p12_01_6]">
                            <label class="custom-control-label" for="p12_01_6">MVCS</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p12_01_7" disabled name="Encuesta01[p12_01_7]">
                            <label class="custom-control-label" for="p12_01_7">RENIEC</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p12_01_8" disabled name="Encuesta01[p12_01_8]">
                            <label class="custom-control-label" for="p12_01_8">Gobierno Regional/gerencia de desarrollo social</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p12_01_9" disabled name="Encuesta01[p12_01_9]">
                            <label class="custom-control-label" for="p12_01_9">DIRESA /GERESA</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p12_01_10" disabled name="Encuesta01[p12_01_10]">
                            <label class="custom-control-label" for="p12_01_10">Dirección Regional de Educación</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p12_01_11" disabled name="Encuesta01[p12_01_11]">
                            <label class="custom-control-label" for="p12_01_11">GOBIERNO LOCAL</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p12_01_12" disabled name="Encuesta01[p12_01_12]">
                            <label class="custom-control-label" for="p12_01_12">ONG</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p12_01_13" disabled name="Encuesta01[p12_01_13]">
                            <label class="custom-control-label" for="p12_01_13">OTROS</label>
                        </div>
                    </div>
                    <div class="p12_01_13 " style="display:none">
                        <input type="text" class="form-control error_p12_01_13" name="Encuesta01[p12_01_13_otros]" disabled id="p12_01_13_otros">
                    </div>
                    

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="card overflow-hidden">
                <div class="card-body text-justify">
                    <label>
                        13. En el último año, ¿Usted identificó la presencia de Organismos No Gubernamentales (ONG) realizando actividades en del ámbito de influencia de los Tambos? <span class="text-danger">*</span> <br>
                        <!-- <small>Este indicador permite identificar a ONGs dentro del ámbito de influencia de los tambos para integrarlos en actividades vinculantes a la estrategia de Tambo Bicentenario, tales como Organismos No Gubernamentales en último año. </small> -->
                    </label>
                    <div class="form-inline error_p13">
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                            <input type="radio" id="p13_1" name="Encuesta01[p13]" value="1" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p13_1">Si</label>
                        </div>
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                            <input type="radio" id="p13_2" name="Encuesta01[p13]" value="2" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p13_2">No</label>
                        </div>
                    </div>
                    <div class="mt-3 p13" style="display:none">
                        <label>ONG:</label>
                        <input type="text" class="form-control error_p13_ong" name="Encuesta01[p13_ong]" disabled id="p13_ong">
                        <label>Tema:</label>
                        <input type="text" class="form-control error_p13_tema" name="Encuesta01[p13_tema]" disabled id="p13_tema">
                    </div>
                    

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="card overflow-hidden">
                <div class="card-body text-justify">
                    <label>
                        14. En el último año, ¿Usted identificó la presencia  empresas privada, dentro del ámbito de influencia de los Tambos?  <span class="text-danger">*</span> <br>
                        <small>Este indicador permite identificar a otros actores del sector privada dentro del ámbito de influencia de los tambos para integrarlos en actividades vinculantes a la estrategia de Tambo Bicentenario, empresas privadas como mineras y otras.</small>
                    </label>
                    <div class="form-inline error_p14">
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                            <input type="radio" id="p14_1" name="Encuesta01[p14]" value="1" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p14_1">Si</label>
                        </div>
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                            <input type="radio" id="p14_2" name="Encuesta01[p14]" value="2" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p14_2">No</label>
                        </div>
                    </div>
                    <div class="mt-3 p14" style="display:none">
                        <label>Empresa:</label>
                        <input type="text" class="form-control error_p14_empresa" name="Encuesta01[p14_empresa]" disabled id="p14_empresa">
                    </div>

                    <!-- <div class="mt-3 p14" style="display:none">
                        <label>Componente:</label>
                        <select name="Encuesta01[p14_componente]" id="p14_componente" class="form-control error_p14_componente">
                            <option value>Seleccionar</option>
                            <option value="MODULO DE DESARROLLO INFANTIL TEMPRANO">MÓDULO DE DESARROLLO INFANTIL TEMPRANO</option>
                            <option value="CENTRO DE INNOVACION">CENTRO DE INNOVACION</option>
                            <option value="MÓDULO DE DESARROLLO PRODUCTIVO E INCLUSIÓN FINANCIERA HAMBRE CERO">MÓDULO DE DESARROLLO PRODUCTIVO E INCLUSIÓN FINANCIERA HAMBRE CERO</option>
                            <option value="GESTORES CON COMPETENCIAS">GESTORES CON COMPETENCIAS</option>
                        </select>
                    </div> -->
                </div>
            </div>
        </div>
    </div>

    <div class="row p14_lista" style="display:none">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="card overflow-hidden">
                <div class="card-body text-justify">
                    <label>
                        14. 1. Listado de componentes (Seleccione una o varias alternativas) <br>
                    </label>
                    <div class="error_p14_lista">
                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p14_01_1" disabled name="Encuesta01[p14_01_1]">
                            <label class="custom-control-label" for="p14_01_1">MÓDULO DE DESARROLLO INFANTIL TEMPRANO</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p14_01_2" disabled name="Encuesta01[p14_01_2]">
                            <label class="custom-control-label" for="p14_01_2">CENTRO DE INNOVACION</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p14_01_3" disabled name="Encuesta01[p14_01_3]">
                            <label class="custom-control-label" for="p14_01_3">MÓDULO DE DESARROLLO PRODUCTIVO E INCLUSIÓN FINANCIERA HAMBRE CERO</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p14_01_4" disabled name="Encuesta01[p14_01_4]">
                            <label class="custom-control-label" for="p14_01_4">GESTORES CON COMPETENCIAS</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="card overflow-hidden">
                <div class="card-body text-justify">
                    <label>
                        15.	En el último año, ¿Usted identifico la presencia a Universidad públicas o privadas en el ámbito de influencia del Tambo para integrarlos en actividades vinculantes al Tambo? <span class="text-danger">*</span> <br>
                        <!-- <small>Este indicador permite identificar a otros actores dentro del ámbito de influencia de los tambos para integrarlos en actividades vinculantes a la estrategia de Tambo Bicentenario, como son las universidades públicas_____ o Universidades privadas____ en el marco de la responsabilidad social universitaria.</small> -->
                    </label>
                    <div class="form-inline error_p15">
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                            <input type="radio" id="p15_1" name="Encuesta01[p15]" value="1" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p15_1">Si</label>
                        </div>
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                            <input type="radio" id="p15_2" name="Encuesta01[p15]" value="2" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p15_2">No</label>
                        </div>
                    </div>
                    <div class="mt-3 p15" style="display:none">
                        <label>Universidad:</label>
                        <input type="text" class="form-control error_p15_universidad" name="Encuesta01[p15_universidad]" disabled id="p15_universidad">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="card overflow-hidden">
                <div class="card-body text-justify">
                    <label>
                        16.	En el último año, ¿Usted identificó la presencia de otros ministerios o instituciones públicas tales como (MINEDU, MINSA, MINCUL, MIMP., MINJUS, MINTRA) desarrollando actividades en el ámbito de influencia del Tambo? <span class="text-danger">*</span> <br>
                        <!-- <small>Este indicador permite identificar otros ministerios en actividades vinculantes a la estrategia de Tambo Bicentenario, tales como (MINEDU, MINSA, MINCUL, MIMP., MINJUS, MINTRA). </small> -->
                    </label>
                    <div class="form-inline error_p16">
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                            <input type="radio" id="p16_1" name="Encuesta01[p16]" value="1" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p16_1">Si</label>
                        </div>
                        <div class="custom-control custom-radio custom-radio-outline custom-radio-primary  mr-3">
                            <input type="radio" id="p16_2" name="Encuesta01[p16]" value="2" disabled class="custom-control-input">
                            <label class="custom-control-label" for="p16_2">No</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row p16_lista" style="display:none">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="card overflow-hidden">
                <div class="card-body text-justify">
                    <label>
                        16. 1. Listado de ministerios y/o instituciones públicas que desarrollan actividades en el ámbito de influencia del TAMBO. (Seleccione una o varias alternativas) <br>
                    </label>
                    <div class="error_p16_lista">
                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p16_01_1" disabled name="Encuesta01[p16_01_1]">
                            <label class="custom-control-label" for="p16_01_1">MINEDU</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p16_01_2" disabled name="Encuesta01[p16_01_2]">
                            <label class="custom-control-label" for="p16_01_2">MINSA</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p16_01_3" disabled name="Encuesta01[p16_01_3]">
                            <label class="custom-control-label" for="p16_01_3">MINCUL</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p16_01_4" disabled name="Encuesta01[p16_01_4]">
                            <label class="custom-control-label" for="p16_01_4">MIMP</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p16_01_5" disabled name="Encuesta01[p16_01_5]">
                            <label class="custom-control-label" for="p16_01_5">MINJUS</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p16_01_6" disabled name="Encuesta01[p16_01_6]">
                            <label class="custom-control-label" for="p16_01_6">MINTRA</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="p16_01_7" disabled name="Encuesta01[p16_01_7]">
                            <label class="custom-control-label" for="p16_01_7">OTROS</label>
                        </div>
                    </div>
                    <div class="p16_01_7" style="display:none">
                        <input type="text" class="form-control error_p16_01_7" name="Encuesta01[p16_01_7_otros]" disabled id="p16_01_7_otros">
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="card overflow-hidden">
                <div class="card-body text-justify">
                    <label>
                    17. Comentarios y observaciones.
                    </label>
                    <textarea class="form-control" name="Encuesta01[p17]" id="p17" disabled rows="5"></textarea>
                </div>
            </div>
        </div>
    </div>


</div>
<!-- container-fluid -->
<script>

var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var snip = "<?= $snip ?>";


DatosGenerales()
async function DatosGenerales(){

    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/encuesta/datos-generales',
        method: 'POST',
        data:{"_csrf":csrf,"snip":snip},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {
            if(results.success){
                $('.TAMBO').html(results.data.TAMBO);
                $('.DEPARTAMENTO').html(results.data.DEPARTAMENTO);
                $('.PROVINCIA').html(results.data.PROVINCIA);
                $('.DISTRITO').html(results.data.DISTRITO);
                $('.GESTOR').html(results.data.GESTOR);
                $('.DNI').html(results.data.DNI);
            }
        },
        error:function(){
            alert('No hay conectividad con el sistema');
        }
    });
}

InformacionEncuesta()
async function InformacionEncuesta(){

    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/encuesta/informacion-encuesta',
        method: 'POST',
        data:{"_csrf":csrf,"snip":snip},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {
            if(results.success){
                $("[name=\"Encuesta01[p01]\"][value='" + results.data.p01 + "']").prop("checked", true);
                $("[name=\"Encuesta01[p02]\"][value='" + results.data.p02 + "']").prop("checked", true);
                $("[name=\"Encuesta01[p03]\"][value='" + results.data.p03 + "']").prop("checked", true);
                if(results.data.p03 == "1" || results.data.p03 == "2"){
                    $('.p03').show();
                    $("[name=\"Encuesta01[p03_velocidad]\"]").val(results.data.p03_velocidad);
                }
                $("[name=\"Encuesta01[p04]\"][value='" + results.data.p04 + "']").prop("checked", true);
                $("[name=\"Encuesta01[p05]\"][value='" + results.data.p05 + "']").prop("checked", true);
                $("[name=\"Encuesta01[p06]\"][value='" + results.data.p06 + "']").prop("checked", true);
                $("[name=\"Encuesta01[p07]\"][value='" + results.data.p07 + "']").prop("checked", true);
                $("[name=\"Encuesta01[p08]\"][value='" + results.data.p08 + "']").prop("checked", true);
                $("[name=\"Encuesta01[p09]\"][value='" + results.data.p09 + "']").prop("checked", true);
                $("[name=\"Encuesta01[p10]\"][value='" + results.data.p10 + "']").prop("checked", true);
                if(results.data.p10 == "1"){
                    $('.p10_lista').show();
                    if(results.data.p10_01_1 == "1"){
                        $("[name=\"Encuesta01[p10_01_1]\"]").prop("checked", true);
                    }

                    if(results.data.p10_01_2 == "1"){
                        $("[name=\"Encuesta01[p10_01_2]\"]").prop("checked", true);
                    }

                    if(results.data.p10_01_3 == "1"){
                        $("[name=\"Encuesta01[p10_01_3]\"]").prop("checked", true);
                    }

                    if(results.data.p10_01_4 == "1"){
                        $("[name=\"Encuesta01[p10_01_4]\"]").prop("checked", true);
                    }

                    if(results.data.p10_01_5 == "1"){
                        $("[name=\"Encuesta01[p10_01_5]\"]").prop("checked", true);
                    }

                    if(results.data.p10_01_6 == "1"){
                        $("[name=\"Encuesta01[p10_01_6]\"]").prop("checked", true);
                    }

                    if(results.data.p10_01_7 == "1"){
                        $("[name=\"Encuesta01[p10_01_7]\"]").prop("checked", true);
                    }

                    if(results.data.p10_01_8 == "1"){
                        $("[name=\"Encuesta01[p10_01_8]\"]").prop("checked", true);
                    }

                    if(results.data.p10_01_9 == "1"){
                        $("[name=\"Encuesta01[p10_01_9]\"]").prop("checked", true);
                    }

                    if(results.data.p10_01_10 == "1"){
                        $("[name=\"Encuesta01[p10_01_10]\"]").prop("checked", true);
                    }

                    if(results.data.p10_01_11 == "1"){
                        $("[name=\"Encuesta01[p10_01_11]\"]").prop("checked", true);
                    }

                    if(results.data.p10_01_12 == "1"){
                        $("[name=\"Encuesta01[p10_01_12]\"]").prop("checked", true);
                    }

                    if(results.data.p10_01_13 == "1"){
                        $("[name=\"Encuesta01[p10_01_13]\"]").prop("checked", true);
                    }

                    if(results.data.p10_01_14 == "1"){
                        $("[name=\"Encuesta01[p10_01_14]\"]").prop("checked", true);
                    }

                    if(results.data.p10_01_15 == "1"){
                        $("[name=\"Encuesta01[p10_01_15]\"]").prop("checked", true);
                        $('.p10_01_15').show();
                        $("[name=\"Encuesta01[p10_01_15_otros]\"]").val(results.data.p10_01_15_otros);
                    }
                }

                $("[name=\"Encuesta01[p11]\"][value='" + results.data.p11 + "']").prop("checked", true);
                $("[name=\"Encuesta01[p12]\"][value='" + results.data.p12 + "']").prop("checked", true);

                if(results.data.p12 == "1"){
                    $('.p12_lista').show();

                    if(results.data.p12_01_1 == "1"){
                        $("[name=\"Encuesta01[p12_01_1]\"]").prop("checked", true);
                    }

                    if(results.data.p12_01_2 == "1"){
                        $("[name=\"Encuesta01[p12_01_2]\"]").prop("checked", true);
                    }

                    if(results.data.p12_01_3 == "1"){
                        $("[name=\"Encuesta01[p12_01_3]\"]").prop("checked", true);
                    }

                    if(results.data.p12_01_4 == "1"){
                        $("[name=\"Encuesta01[p12_01_4]\"]").prop("checked", true);
                    }
                    
                    if(results.data.p12_01_5 == "1"){
                        $("[name=\"Encuesta01[p12_01_5]\"]").prop("checked", true);
                    }

                    if(results.data.p12_01_6 == "1"){
                        $("[name=\"Encuesta01[p12_01_6]\"]").prop("checked", true);
                    }

                    if(results.data.p12_01_7 == "1"){
                        $("[name=\"Encuesta01[p12_01_7]\"]").prop("checked", true);
                    }

                    if(results.data.p12_01_8 == "1"){
                        $("[name=\"Encuesta01[p12_01_8]\"]").prop("checked", true);
                    }

                    if(results.data.p12_01_9 == "1"){
                        $("[name=\"Encuesta01[p12_01_9]\"]").prop("checked", true);
                    }

                    if(results.data.p12_01_10 == "1"){
                        $("[name=\"Encuesta01[p12_01_10]\"]").prop("checked", true);
                    }

                    if(results.data.p12_01_11 == "1"){
                        $("[name=\"Encuesta01[p12_01_11]\"]").prop("checked", true);
                    }

                    if(results.data.p12_01_12 == "1"){
                        $("[name=\"Encuesta01[p12_01_12]\"]").prop("checked", true);
                    }

                    if(results.data.p12_01_13 == "1"){
                        $("[name=\"Encuesta01[p12_01_13]\"]").prop("checked", true);
                        $('.p12_01_13 ').show();
                        $("[name=\"Encuesta01[p12_01_13_otros]\"]").val(results.data.p12_01_13_otros);
                    }
                }
                $("[name=\"Encuesta01[p13]\"][value='" + results.data.p13 + "']").prop("checked", true);

                if(results.data.p13 == "1"){
                    $('.p13').show();
                    $("[name=\"Encuesta01[p13_ong]\"]").val(results.data.p13_ong);
                    $("[name=\"Encuesta01[p13_tema]\"]").val(results.data.p13_tema);
                }

                $("[name=\"Encuesta01[p14]\"][value='" + results.data.p14 + "']").prop("checked", true);

                if(results.data.p14 == "1"){
                    $('.p14_lista').show();
                    $('.p14').show();
                    $("[name=\"Encuesta01[p14_empresa]\"]").val(results.data.p14_empresa);

                    if(results.data.p14_01_1 == "1"){
                        $("[name=\"Encuesta01[p14_01_1]\"]").prop("checked", true);
                    }

                    if(results.data.p14_01_2 == "1"){
                        $("[name=\"Encuesta01[p14_01_2]\"]").prop("checked", true);
                    }

                    if(results.data.p14_01_3 == "1"){
                        $("[name=\"Encuesta01[p14_01_3]\"]").prop("checked", true);
                    }

                    if(results.data.p14_01_4 == "1"){
                        $("[name=\"Encuesta01[p14_01_4]\"]").prop("checked", true);
                    }

                }


                $("[name=\"Encuesta01[p15]\"][value='" + results.data.p15 + "']").prop("checked", true);

                if(results.data.p15 == "1"){
                    $('.p15').show();
                    $("[name=\"Encuesta01[p15_universidad]\"]").val(results.data.p15_universidad);
                }


                $("[name=\"Encuesta01[p16]\"][value='" + results.data.p16 + "']").prop("checked", true);

                if(results.data.p16 == "1"){
                    $('.p16_lista').show();
                    if(results.data.p16_01_1 == "1"){
                        $("[name=\"Encuesta01[p16_01_1]\"]").prop("checked", true);
                    }

                    if(results.data.p16_01_2 == "1"){
                        $("[name=\"Encuesta01[p16_01_2]\"]").prop("checked", true);
                    }

                    if(results.data.p16_01_3 == "1"){
                        $("[name=\"Encuesta01[p16_01_3]\"]").prop("checked", true);
                    }

                    if(results.data.p16_01_4 == "1"){
                        $("[name=\"Encuesta01[p16_01_4]\"]").prop("checked", true);
                    }
                    
                    if(results.data.p16_01_5 == "1"){
                        $("[name=\"Encuesta01[p16_01_5]\"]").prop("checked", true);
                    }

                    if(results.data.p16_01_6 == "1"){
                        $("[name=\"Encuesta01[p16_01_6]\"]").prop("checked", true);
                    }

                    if(results.data.p16_01_7 == "1"){
                        $("[name=\"Encuesta01[p16_01_7]\"]").prop("checked", true);
                        $('.p16_01_7').show();
                        $("[name=\"Encuesta01[p16_01_7_otros]\"]").val(results.data.p16_01_7_otros);
                    }
                }

                $("[name=\"Encuesta01[p17]\"]").val(results.data.p17);

                // $('.TAMBO').html(results.data.TAMBO);
                // $('.DEPARTAMENTO').html(results.data.DEPARTAMENTO);
                // $('.PROVINCIA').html(results.data.PROVINCIA);
                // $('.DISTRITO').html(results.data.DISTRITO);
                // $('.GESTOR').html(results.data.GESTOR);
                // $('.DNI').html(results.data.DNI);
            }
            console.log(results);
        },
        error:function(){
            alert('No hay conectividad con el sistema');
        }
    });
}


</script>