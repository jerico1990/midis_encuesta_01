<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "encuesta_01".
 *
 * @property int $id
 * @property int $snip
 * @property string|null $p01
 * @property string|null $p02
 * @property string|null $p03
 * @property string|null $p03_velocidad
 * @property string|null $p04
 * @property string|null $p05
 * @property string|null $p06
 * @property string|null $p07
 * @property string|null $p08
 * @property string|null $p09
 * @property string|null $p10
 * @property string|null $p10_01_1
 * @property string|null $p10_01_2
 * @property string|null $p10_01_3
 * @property string|null $p10_01_4
 * @property string|null $p10_01_5
 * @property string|null $p10_01_6
 * @property string|null $p10_01_7
 * @property string|null $p10_01_8
 * @property string|null $p10_01_9
 * @property string|null $p10_01_10
 * @property string|null $p10_01_11
 * @property string|null $p10_01_12
 * @property string|null $p10_01_13
 * @property string|null $p10_01_14
 * @property string|null $p10_01_15
 * @property string|null $p10_01_15_otros
 * @property string|null $p11
 * @property string|null $p12
 * @property string|null $p12_01_1
 * @property string|null $p12_01_2
 * @property string|null $p12_01_3
 * @property string|null $p12_01_4
 * @property string|null $p12_01_5
 * @property string|null $p12_01_6
 * @property string|null $p12_01_7
 * @property string|null $p12_01_8
 * @property string|null $p12_01_9
 * @property string|null $p12_01_10
 * @property string|null $p12_01_11
 * @property string|null $p12_01_12
 * @property string|null $p12_01_13
 * @property string|null $p12_01_13_otros
 * @property string|null $p13
 * @property string|null $p13_ong
 * @property string|null $p13_tema
 * @property string|null $p14
 * @property string|null $p14_componente
 * @property string|null $p15
 * @property string|null $p15_universidad
 * @property string|null $p16
 * @property string|null $p16_01_1
 * @property string|null $p16_01_2
 * @property string|null $p16_01_3
 * @property string|null $p16_01_4
 * @property string|null $p16_01_5
 * @property string|null $p16_01_6
 * @property string|null $p16_01_7
 * @property string|null $p16_01_7_otros
 * @property string|null $p17
 * @property string $actualizado_en
 * @property int $actualizado_por
 * @property string $creado_en
 * @property int $creado_por
 */
class Encuesta01 extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'encuestas.encuesta_01';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['snip'], 'required'],
            [['snip', 'actualizado_por', 'creado_por'], 'integer'],
            [['p17'], 'string'],
            [['actualizado_en', 'creado_en'], 'safe'],
            [['p01', 'p02', 'p03', 'p03_velocidad', 'p04', 'p05', 'p06', 'p07', 'p08', 'p09', 'p10', 'p10_01_1', 'p10_01_2', 'p10_01_3', 'p10_01_4', 'p10_01_5', 'p10_01_6', 'p10_01_7', 'p10_01_8', 'p10_01_9', 'p10_01_10', 'p10_01_11', 'p10_01_12', 'p10_01_13', 'p10_01_14', 'p10_01_15', 'p10_01_15_otros', 'p11', 'p12', 'p12_01_1', 'p12_01_2', 'p12_01_3', 'p12_01_4', 'p12_01_5', 'p12_01_6', 'p12_01_7', 'p12_01_8', 'p12_01_9', 'p12_01_10', 'p12_01_11', 'p12_01_12', 'p12_01_13', 'p12_01_13_otros', 'p13', 'p13_ong', 'p13_tema', 'p14', 'p14_empresa', 'p14_01_1' , 'p14_01_2' , 'p14_01_3' , 'p14_01_4' , 'p15', 'p15_universidad', 'p16', 'p16_01_1', 'p16_01_2', 'p16_01_3', 'p16_01_4', 'p16_01_5', 'p16_01_6', 'p16_01_7', 'p16_01_7_otros'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'snip' => 'Snip',
            'p01' => 'P01',
            'p02' => 'P02',
            'p03' => 'P03',
            'p03_velocidad' => 'P03 Velocidad',
            'p04' => 'P04',
            'p05' => 'P05',
            'p06' => 'P06',
            'p07' => 'P07',
            'p08' => 'P08',
            'p09' => 'P09',
            'p10' => 'P10',
            'p10_01_1' => 'P10 01 1',
            'p10_01_2' => 'P10 01 2',
            'p10_01_3' => 'P10 01 3',
            'p10_01_4' => 'P10 01 4',
            'p10_01_5' => 'P10 01 5',
            'p10_01_6' => 'P10 01 6',
            'p10_01_7' => 'P10 01 7',
            'p10_01_8' => 'P10 01 8',
            'p10_01_9' => 'P10 01 9',
            'p10_01_10' => 'P10 01 10',
            'p10_01_11' => 'P10 01 11',
            'p10_01_12' => 'P10 01 12',
            'p10_01_13' => 'P10 01 13',
            'p10_01_14' => 'P10 01 14',
            'p10_01_15' => 'P10 01 15',
            'p10_01_15_otros' => 'P10 01 15 Otros',
            'p11' => 'P11',
            'p12' => 'P12',
            'p12_01_1' => 'P12 01 1',
            'p12_01_2' => 'P12 01 2',
            'p12_01_3' => 'P12 01 3',
            'p12_01_4' => 'P12 01 4',
            'p12_01_5' => 'P12 01 5',
            'p12_01_6' => 'P12 01 6',
            'p12_01_7' => 'P12 01 7',
            'p12_01_8' => 'P12 01 8',
            'p12_01_9' => 'P12 01 9',
            'p12_01_10' => 'P12 01 10',
            'p12_01_11' => 'P12 01 11',
            'p12_01_12' => 'P12 01 12',
            'p12_01_13' => 'P12 01 13',
            'p12_01_13_otros' => 'P12 01 13 Otros',
            'p13' => 'P13',
            'p13_ong' => 'P13 Ong',
            'p13_tema' => 'P13 Tema',
            'p14' => 'P14',
            'p14_componente' => 'P14 Componente',
            'p15' => 'P15',
            'p15_universidad' => 'P15 Universidad',
            'p16' => 'P16',
            'p16_01_1' => 'P16 01 1',
            'p16_01_2' => 'P16 01 2',
            'p16_01_3' => 'P16 01 3',
            'p16_01_4' => 'P16 01 4',
            'p16_01_5' => 'P16 01 5',
            'p16_01_6' => 'P16 01 6',
            'p16_01_7' => 'P16 01 7',
            'p16_01_7_otros' => 'P16 01 7 Otros',
            'p17' => 'P17',
            'actualizado_en' => 'Actualizado En',
            'actualizado_por' => 'Actualizado Por',
            'creado_en' => 'Creado En',
            'creado_por' => 'Creado Por',
        ];
    }
}
