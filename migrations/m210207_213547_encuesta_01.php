<?php

use yii\db\Migration;

/**
 * Class m210207_213547_encuesta_01
 */
class m210207_213547_encuesta_01 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210207_213547_encuesta_01 cannot be reverted.\n";

        return false;
    }

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('encuestas.encuesta_01', [
            'id'                => $this->primaryKey(),
            'snip'              => $this->integer()->notNull(),
            'p01'               => $this->string(),
            'p02'               => $this->string(),
            'p03'               => $this->string(),
            'p03_velocidad'     => $this->string(),
            'p04'               => $this->string(),
            'p05'               => $this->string(),
            'p06'               => $this->string(),
            'p07'               => $this->string(),
            'p08'               => $this->string(),
            'p09'               => $this->string(),
            'p10'               => $this->string(),
            'p10_01_1'          => $this->string(),
            'p10_01_2'          => $this->string(),
            'p10_01_3'          => $this->string(),
            'p10_01_4'          => $this->string(),
            'p10_01_5'          => $this->string(),
            'p10_01_6'          => $this->string(),
            'p10_01_7'          => $this->string(),
            'p10_01_8'          => $this->string(),
            'p10_01_9'          => $this->string(),
            'p10_01_10'         => $this->string(),
            'p10_01_11'         => $this->string(),
            'p10_01_12'         => $this->string(),
            'p10_01_13'         => $this->string(),
            'p10_01_14'         => $this->string(),
            'p10_01_15'         => $this->string(),
            'p10_01_15_otros'   => $this->string(),
            'p11'               => $this->string(),
            'p12'               => $this->string(),
            'p12_01_1'          => $this->string(),
            'p12_01_2'          => $this->string(),
            'p12_01_3'          => $this->string(),
            'p12_01_4'          => $this->string(),
            'p12_01_5'          => $this->string(),
            'p12_01_6'          => $this->string(),
            'p12_01_7'          => $this->string(),
            'p12_01_8'          => $this->string(),
            'p12_01_9'          => $this->string(),
            'p12_01_10'         => $this->string(),
            'p12_01_11'         => $this->string(),
            'p12_01_12'         => $this->string(),
            'p12_01_13'         => $this->string(),
            'p12_01_13_otros'   => $this->string(),
            'p13'               => $this->string(),
            'p13_ong'           => $this->string(),
            'p13_tema'          => $this->string(),
            'p14'               => $this->string(),
            'p14_empresa'       => $this->string(),
            'p14_01_1'          => $this->string(),
            'p14_01_2'          => $this->string(),
            'p14_01_3'          => $this->string(),
            'p14_01_4'          => $this->string(),
            'p15'               => $this->string(),
            'p15_universidad'   => $this->string(),
            'p16'               => $this->string(),
            'p16_01_1'          => $this->string(),
            'p16_01_2'          => $this->string(),
            'p16_01_3'          => $this->string(),
            'p16_01_4'          => $this->string(),
            'p16_01_5'          => $this->string(),
            'p16_01_6'          => $this->string(),
            'p16_01_7'          => $this->string(),
            'p16_01_7_otros'    => $this->string(),
            'p17'               => $this->text(),
            'actualizado_en'    => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'actualizado_por'   => $this->integer(11),
            'creado_en'         => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'creado_por'        => $this->integer(11),
        ]);
    }

    public function down()
    {
        $this->dropTable('encuestas.encuesta_01');
    }
}
