<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Encuesta01;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
// use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
// use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class EncuestaController extends Controller
{
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','validacion','datos-generales','ver','editar','reporte-excel'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'validacion','datos-generales','ver','editar','reporte-excel'],
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index'             => ['post','get'],
                    'validacion'        => ['post'],
                    'datos-generales'   => ['post'],
                    'ver'               => ['post','get'],
                    'editar'            => ['post','get'],
                    'reporte-excel'     => ['post','get'],
                ],
            ],
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to
                    'Origin' => ['https://www.pais.gob.pe', 'http://localhost','http://localhost:4200','https://www.myserver.com'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Method' => ['POST','PUT','GET'],
                    // // Allow only headers 'X-Wsse'
                    'Access-Control-Request-Headers' => ['X-Wsse'],
                    // // Allow credentials (cookies, authorization headers, etc.) to be exposed to the browser
                    'Access-Control-Allow-Credentials' => true,
                    // // Allow OPTIONS caching
                    'Access-Control-Max-Age' => 3600,
                    // // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],
    
            ],
        ];
    }

    public function beforeAction($action)
    {            
        if ($action->id == 'validacion') {
            $this->enableCsrfValidation = false;
        }

        if ($action->id == 'index') {
            $this->enableCsrfValidation = false;
        }

        if ($action->id == 'datos-generales') {
            $this->enableCsrfValidation = false;
        }

        if ($action->id == 'informacion-encuesta') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($snip=null,$hash=null)
    {
        $request = Yii::$app->request;
        $this->layout='skote';
        
        if(!$snip || !$hash){
            return $this->render('no-autorizado');
        }

        if(!$hash || !Yii::$app->security->validatePassword("encuesta-2021",$hash)){
            return $this->render('no-autorizado');
        }

        $model = new Encuesta01();
        $encuesta = (new \yii\db\Query())->from('encuestas.encuesta_01')->andWhere(['=', "snip",$snip])->count();
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post())){
                $model->actualizado_en = date('Y-m-d H:i:s');
                $model->creado_en = date('Y-m-d H:i:s');
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
                
            }
        }else{
            // if($encuesta==1){
            //     return $this->redirect(['ver','snip'=>$snip]);
            // }else{
                return $this->render('index',['snip'=>$snip,'encuesta'=>$encuesta]);
            //}
            
        }
    }

    public function actionVer($snip=null)
    {
        $request = Yii::$app->request;
        $this->layout='skote';

        return $this->render('ver',['snip'=>$snip]);
    }
    
    public function actionReporte()
    {
        $request = Yii::$app->request;
        $this->layout='skote';

        return $this->render('reporte');
    }

    public function actionEditar($snip=null)
    {
        $request = Yii::$app->request;
        $this->layout='skote';
        $model = Encuesta01::find()->where(['snip' => $snip])->one();
        
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post())){
                $model->actualizado_en = date('Y-m-d H:i:s');
                $model->creado_en = date('Y-m-d H:i:s');
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
                
            }
        }else{
            // if($encuesta==1){
            //     return $this->redirect(['ver','snip'=>$snip]);
            // }else{
                return $this->render('editar',['snip'=>$snip]);
            //}
            
        }
    }

    public function actionValidacion(){
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
       
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            
            $snip = (isset($_POST['snip']) && $_POST['snip']!='')?$_POST['snip']:null;

            $model = (new \yii\db\Query())
                ->from('encuestas.encuesta_01');
            $model = $model->andWhere(['=', "snip",$snip]);
            $model = $model->count();
            if($model>=1){
                return ['success'=>true];
            }else{
                return ['success'=>false];
            }
        }
    }

    public function actionInformacionEncuesta(){
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
       
        if($request->isAjax && $_SERVER['REQUEST_METHOD'] == 'POST'){
            
            $snip = (isset($_POST['snip']) && $_POST['snip']!='')?$_POST['snip']:null;

            $informacion = (new \yii\db\Query())->select('*')->from('encuestas.encuesta_01');
            $informacion = $informacion->andWhere(['=', "SNIP",$snip]);
            $informacion = $informacion->one();

            return ['success'=>true,'data'=>$informacion];
        }
    }


    public function actionDatosGenerales(){
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
       
        if($request->isAjax && $_SERVER['REQUEST_METHOD'] == 'POST'){
            
            $snip = (isset($_POST['snip']) && $_POST['snip']!='')?$_POST['snip']:null;

            $model = (new \yii\db\Query())->select('DEPARTAMENTO,PROVINCIA,DISTRITO,TAMBO,GESTOR,DNI')->from('maestras.vw_datos_general_tambos_gestores');
            $model = $model->andWhere(['=', "SNIP",$snip]);
            $model = $model->one();

            return ['success'=>true,'data'=>$model];
        }
    }

    public function actionDatosGeneralesEncuestas(){
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
       
        if($request->isAjax && $_SERVER['REQUEST_METHOD'] == 'POST'){
            
            $model = (new \yii\db\Query())->select('*')->from('encuestas.VW_ENCUESTA_ENCUESTA01');
            $model = $model->all();

            return ['success'=>true,'data'=>$model];
        }
    }

    public function actionReporteExcel(){
        $inputFileName = \Yii::$app->basePath.'/plantilla/plantilla_encuestas_midis.xlsx';
        $inputFileType = IOFactory::identify($inputFileName);
        $reader = IOFactory::createReader($inputFileType);

        // /** Load $inputFileName to a Spreadsheet Object  **/
        $spread = $reader->load($inputFileName);
        #$spread = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);
        //$spread = new Spreadsheet();
        

        // $a = 3;
        // $spread->getActiveSheet()
        //         ->setCellValue('B'.$a, 'Nombre - Tambo')
        //         ->setCellValue('C'.$a, 'Departamento')
        //         ->setCellValue('D'.$a, 'Provincia')
        //         ->setCellValue('E'.$a, 'Distrito')
        //         ->setCellValue('F'.$a, 'DNI del evaluador')
        //         ->setCellValue('G'.$a, 'Apellidos y nombres del evaluador')
        //         ->setCellValue('H'.$a, '1. ¿Las vías y medios de transporte son accesibles para llegar al tambo, considerando el tiempo y distancia?')
        //         ->setCellValue('I'.$a, '2. ¿Cuenta con servicio de Agua?')
        //         ->setCellValue('J'.$a, '3. ¿Cuenta con servicio de Internet?')
        //         ->setCellValue('K'.$a, '3. 1.¿Cuál es la velocidad de internet? (Para realizar la medición del internte ingrese al siguiente enlace http://fast.com )')
        //         ->setCellValue('L'.$a, '4. ¿Cuenta con servicio de telefonía (móvil o fija)?')
        //         ->setCellValue('M'.$a, '5. ¿Cuenta con Energía Eléctrica?')
        //         ->setCellValue('N'.$a, '6. ¿Servicio de Guardianía?')
        //         ->setCellValue('O'.$a, '7. ¿Los ambientes (oficina, dormitorio, cocina,zum) están operativos?')
        //         ->setCellValue('P'.$a, '8. ¿El Tambo cuenta con acceso a agua de riego?')
        //         ->setCellValue('Q'.$a, '9. En el último año, ¿Usted identifica presencia del Gobierno Regional en ámbito de Tambo?')
        //         ->setCellValue('R'.$a, '10. En el último año, ¿Identifica presencia de instituciones u otros que desarrollen labores en el ámbito productivo, en el ámbito de influencia del Tambo?')
        //         ->setCellValue('S'.$a, 'MIDAGRI')
        //         ->setCellValue('T'.$a, 'AGROBANCO')
        //         ->setCellValue('U'.$a, 'AGROIDEAS')
        //         ->setCellValue('V'.$a, 'AGRORURAL')
        //         ->setCellValue('W'.$a, 'SENASA')
        //         ->setCellValue('X'.$a, 'INIA')
        //         ->setCellValue('Y'.$a, 'PRODUCE')
        //         ->setCellValue('Z'.$a, 'MINCETUR')
        //         ->setCellValue('AA'.$a, 'GERENCIA REGIONAL DE DESARROLLO ECONÓMICO')
        //         ->setCellValue('AB'.$a, 'DIRECCIÓN REGIONAL DE AGRICULTURA')
        //         ->setCellValue('AC'.$a, 'AGENCIA AGRARIA')
        //         ->setCellValue('AD'.$a, 'GOBIERNO LOCAL')
        //         ->setCellValue('AE'.$a, 'ODEL')
        //         ->setCellValue('AF'.$a, 'ONG')
        //         ->setCellValue('AG'.$a, 'OTROS')
        //         ->setCellValue('AH'.$a, 'OTROS DESCRIPCION')
        //         ->setCellValue('AI'.$a, '11. En el último año, ¿La Instancia de Articulación Local (IAL) ha planteado estrategias para asegurar el cumplimiento de la Meta 4: Acciones para promover la adecuada alimentación, y la prevención y reducción de la anemia?')
        //         ->setCellValue('AJ'.$a, '12. En el último año, ¿Usted identifico la presencia de instituciones o programas vinculados al desarrollo infantil temprano en el ámbito de influencia del Tambo?')
        //         ->setCellValue('AK'.$a, 'PN Cuna Mas')
        //         ->setCellValue('AL'.$a, 'PN Juntos')
        //         ->setCellValue('AM'.$a, 'Ministerio de la Mujer')
        //         ->setCellValue('AN'.$a, 'MINSA')
        //         ->setCellValue('AO'.$a, 'MINEDU')
        //         ->setCellValue('AP'.$a, 'MVCS')
        //         ->setCellValue('AQ'.$a, 'RENIEC')
        //         ->setCellValue('AR'.$a, 'Gobierno Regional/gerencia de desarrollo social')
        //         ->setCellValue('AS'.$a, 'DIRESA /GERESA')
        //         ->setCellValue('AT'.$a, 'Dirección Regional de Educación')
        //         ->setCellValue('AU'.$a, 'GOBIERNO LOCAL')
        //         ->setCellValue('AV'.$a, 'ONG')
        //         ->setCellValue('AW'.$a, 'OTROS')
        //         ->setCellValue('AX'.$a, 'OTROS DESCRIPCION')
        //         ->setCellValue('AY'.$a, '13. En el último año, ¿Usted identificó la presencia de Organismos No Gubernamentales (ONG) realizando actividades en del ámbito de influencia de los Tambos?')
        //         ->setCellValue('AZ'.$a, 'ONG')
        //         ->setCellValue('BA'.$a, 'Tema')
        //         ->setCellValue('BB'.$a, '14. En el último año, ¿Usted identificó la presencia empresas privada, dentro del ámbito de influencia de los Tambos?')
        //         ->setCellValue('BC'.$a, 'Empresa')
        //         ->setCellValue('BD'.$a, 'MÓDULO DE DESARROLLO INFANTIL TEMPRANO')
        //         ->setCellValue('BE'.$a, 'CENTRO DE INNOVACION')
        //         ->setCellValue('BF'.$a, 'MÓDULO DE DESARROLLO PRODUCTIVO E INCLUSIÓN FINANCIERA HAMBRE CERO')
        //         ->setCellValue('BG'.$a, 'GESTORES CON COMPETENCIAS')

        //         ->setCellValue('BH'.$a, '15. En el último año, ¿Usted identifico la presencia a Universidad públicas o privadas en el ámbito de influencia del Tambo para integrarlos en actividades vinculantes al Tambo?')
        //         ->setCellValue('BI'.$a, 'Universidad')
        //         // ->setCellValue('BJ'.$a, '16. En el último año, ¿Usted identificó la presencia de otros ministerios o instituciones públicas tales como (MINEDU, MINSA, MINCUL, MIMP., MINJUS, MINTRA) desarrollando actividades en el ámbito de influencia del Tambo?')
        //         // ->setCellValue('BK'.$a, 'MINEDU')
        //         ->setCellValue('BL'.$a, 'MINSA')
        //         ->setCellValue('BM'.$a, 'MINCUL')
        //         ->setCellValue('BN'.$a, 'MIMP')
        //         ->setCellValue('BO'.$a, 'MINJUS')
        //         ->setCellValue('BP'.$a, 'MINTRA')
        //         ->setCellValue('BQ'.$a, 'OTROS')
        //         ->setCellValue('BR'.$a, 'OTROS DESCRIPCION')
        //         ->setCellValue('BS'.$a, '17. Comentarios y observaciones');

        


        $encuestas = (new \yii\db\Query())
            ->select('*')
            ->from('encuestas.VW_ENCUESTA_ENCUESTA01')
            ->all();

        
        $i = 4;
        foreach($encuestas as $encuesta){
            $spread->getActiveSheet()
                ->setCellValue('B'.$i, $encuesta['TAMBO'])
                ->setCellValue('C'.$i, $encuesta['DEPARTAMENTO'])
                ->setCellValue('D'.$i, $encuesta['PROVINCIA'])
                ->setCellValue('E'.$i, $encuesta['DISTRITO'])
                ->setCellValue('F'.$i, $encuesta['DNI'])
                ->setCellValue('G'.$i, $encuesta['GESTOR'])
                ->setCellValue('H'.$i, $encuesta['p01_descripcion'])
                ->setCellValue('I'.$i, $encuesta['p02_descripcion'])
                ->setCellValue('J'.$i, $encuesta['p03_descripcion'])
                ->setCellValue('K'.$i, $encuesta['p03_velocidad'])
                ->setCellValue('L'.$i, $encuesta['p04_descripcion'])
                ->setCellValue('M'.$i, $encuesta['p05_descripcion'])
                ->setCellValue('N'.$i, $encuesta['p06_descripcion'])
                ->setCellValue('O'.$i, $encuesta['p07_descripcion'])
                ->setCellValue('P'.$i, $encuesta['p08_descripcion'])
                ->setCellValue('Q'.$i, $encuesta['p09_descripcion'])
                ->setCellValue('R'.$i, $encuesta['p10_descripcion'])
                ->setCellValue('S'.$i, $encuesta['p10_01_1_descripcion'])
                ->setCellValue('T'.$i, $encuesta['p10_01_2_descripcion'])
                ->setCellValue('U'.$i, $encuesta['p10_01_3_descripcion'])
                ->setCellValue('V'.$i, $encuesta['p10_01_4_descripcion'])
                ->setCellValue('W'.$i, $encuesta['p10_01_5_descripcion'])
                ->setCellValue('X'.$i, $encuesta['p10_01_6_descripcion'])
                ->setCellValue('Y'.$i, $encuesta['p10_01_7_descripcion'])
                ->setCellValue('Z'.$i, $encuesta['p10_01_8_descripcion'])
                ->setCellValue('AA'.$i, $encuesta['p10_01_9_descripcion'])
                ->setCellValue('AB'.$i, $encuesta['p10_01_10_descripcion'])
                ->setCellValue('AC'.$i, $encuesta['p10_01_11_descripcion'])
                ->setCellValue('AD'.$i, $encuesta['p10_01_12_descripcion'])
                ->setCellValue('AE'.$i, $encuesta['p10_01_13_descripcion'])
                ->setCellValue('AF'.$i, $encuesta['p10_01_14_descripcion'])
                ->setCellValue('AG'.$i, $encuesta['p10_01_15_descripcion'])
                ->setCellValue('AH'.$i, $encuesta['p10_01_15_otros'])
                ->setCellValue('AI'.$i, $encuesta['p11_descripcion'])
                ->setCellValue('AJ'.$i, $encuesta['p12_descripcion'])
                ->setCellValue('AK'.$i, $encuesta['p12_01_01_descripcion'])
                ->setCellValue('AL'.$i, $encuesta['p12_01_02_descripcion'])
                ->setCellValue('AM'.$i, $encuesta['p12_01_03_descripcion'])
                ->setCellValue('AN'.$i, $encuesta['p12_01_04_descripcion'])
                ->setCellValue('AO'.$i, $encuesta['p12_01_05_descripcion'])
                ->setCellValue('AP'.$i, $encuesta['p12_01_06_descripcion'])
                ->setCellValue('AQ'.$i, $encuesta['p12_01_07_descripcion'])
                ->setCellValue('AR'.$i, $encuesta['p12_01_08_descripcion'])
                ->setCellValue('AS'.$i, $encuesta['p12_01_09_descripcion'])
                ->setCellValue('AT'.$i, $encuesta['p12_01_10_descripcion'])
                ->setCellValue('AU'.$i, $encuesta['p12_01_11_descripcion'])
                ->setCellValue('AV'.$i, $encuesta['p12_01_12_descripcion'])
                ->setCellValue('AW'.$i, $encuesta['p12_01_13_descripcion'])
                ->setCellValue('AX'.$i, $encuesta['p12_01_13_otros'])
                ->setCellValue('AY'.$i, $encuesta['p13_descripcion'])
                ->setCellValue('AZ'.$i, $encuesta['p13_ong'])
                ->setCellValue('BA'.$i, $encuesta['p13_tema'])
                ->setCellValue('BB'.$i, $encuesta['p14_descripcion'])
                ->setCellValue('BC'.$i, $encuesta['p14_empresa'])
                ->setCellValue('BD'.$i, $encuesta['p14_01_1_descripcion'])
                ->setCellValue('BE'.$i, $encuesta['p14_01_2_descripcion'])
                ->setCellValue('BF'.$i, $encuesta['p14_01_3_descripcion'])
                ->setCellValue('BG'.$i, $encuesta['p14_01_4_descripcion'])
                ->setCellValue('BH'.$i, $encuesta['p15_descripcion'])
                ->setCellValue('BI'.$i, $encuesta['p15_universidad'])
                ->setCellValue('BJ'.$i, $encuesta['p16_descripcion'])
                ->setCellValue('BK'.$i, $encuesta['p16_01_1_descripcion'])
                ->setCellValue('BL'.$i, $encuesta['p16_01_2_descripcion'])
                ->setCellValue('BM'.$i, $encuesta['p16_01_3_descripcion'])
                ->setCellValue('BN'.$i, $encuesta['p16_01_4_descripcion'])
                ->setCellValue('BO'.$i, $encuesta['p16_01_5_descripcion'])
                ->setCellValue('BP'.$i, $encuesta['p16_01_6_descripcion'])
                ->setCellValue('BQ'.$i, $encuesta['p16_01_7_descripcion'])
                ->setCellValue('BR'.$i, $encuesta['p16_01_7_otros'])
                ->setCellValue('BS'.$i, $encuesta['p17']);
            $i++;
        }

        
        #$writer = new Xlsx($spread);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Reporte_Encuesta.xlsx"');
        header('Cache-Control: max-age=0');
        
        #$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spread, 'Xls');
        
        $writer = IOFactory::createWriter($spread, 'Xlsx');
        #$writer->setOffice2003Compatibility(true);

        $response = Yii::$app->getResponse();
        $headers = $response->getHeaders();
        $headers->set('Content-Type', 'application/vnd.ms-excel');
        $headers->set('Content-Disposition', 'attachment;filename="Reporte_Encuesta.xlsx"');

        ob_start();
        $writer->save("php://output");
        $content = ob_get_contents();
        ob_clean();
        return $content;
    }

    public function actionDemo(){
        $spread = new Spreadsheet();

        $sheet = $spread->getActiveSheet();
        $sheet->setTitle("Hoja 1");
        $sheet->setCellValueByColumnAndRow(1, 1, "Valor en la posición 1, 1");

        $sheet->mergeCells("B2:B3");

        $sheet->setCellValue("B2", "Valor en celda B2");
        
        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="demo.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        
        $writer = IOFactory::createWriter($spread, 'Xls');
        $writer->save('php://output');
    }

}
